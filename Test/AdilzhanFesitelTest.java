/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import crypto.AdilzhanFeistel;
import org.junit.Test;
import crypto.Cipher;
import crypto.NPNKey;
import crypto.NPNKeyGenerator;
import crypto.SimpleCipher;
import crypto.SimpleFeistel;
import java.io.FileWriter;
import polynomial.BinaryHelpers;


public class AdilzhanFesitelTest {
    
    //@Test
    public void test32bitNPNS() { 
        String data = "Hell";
	
        System.out.println("Block size is " + data.getBytes().length * 8);
	int[] basesDecimal = {
            67, //1000011 6
            59, //111011 5
            19, //10011 4
            25, //11001 4
            31, //11111 4
            11, //1011 3
            13, //1101 3
            7,  //111 2
            3}; //11 1
	int[] keyDecimal = {31, 19, 15, 13, 11, 7, 6, 3, 3, 1 };
	
	NPNKey fullKey = new NPNKey(basesDecimal, keyDecimal);
	
	Cipher cipher = new SimpleCipher(fullKey, 32);
	
	byte[] crypted = cipher.encrypt(data.getBytes());
	
	byte[] decrypted = cipher.decrypt(crypted);
	
	String output = "";
	
	try {
	    output = new String(decrypted, "UTF-8");
	} catch(Exception e) {
	    System.out.println(e);
	}
	
	org.junit.Assert.assertEquals("failure - strings are not equal", output, data);
    }
    
    @Test
    public void test64bit() {

	String data = "HelloWor";
	int[] rounds = {32};
        for(int roundCount: rounds) {
            System.out.println("Block size is " + data.getBytes().length * 8);
            System.out.println("Input is " + BinaryHelpers.toBinaryString(data.getBytes(), 64));
            System.out.println("IV is " + BinaryHelpers.toBinaryString("1234".getBytes(), 32));
            int[] basesDecimal = {
                67, //1000011 6
                59, //111011 5
                19, //10011 4
                25, //11001 4
                31, //11111 4
                11, //1011 3
                13, //1101 3
                7,  //111 2
                3}; //11 1
            int[] keyDecimal = {31, 19, 15, 13, 11, 7, 6, 3, 3, 1 };

            NPNKey fullKey = new NPNKey(basesDecimal, keyDecimal);

            Cipher cipher = new AdilzhanFeistel(fullKey, 64, roundCount, "1234".getBytes());

            byte[] crypted = cipher.encrypt(data.getBytes());

            byte[] decrypted = cipher.decrypt(crypted);

            String output = "";

            try {
                output = new String(decrypted, "UTF-8");
            } catch(Exception e) {
                System.out.println(e);
            }

            org.junit.Assert.assertEquals("failure - strings are not equal", output, data);
        }
	
    }
    
    @Test
    public void test128bit() {

	String data = "HelloWorHelloWor";
	int[] rounds = {32};
        for(int roundCount: rounds) {
            System.out.println("Block size is " + data.getBytes().length * 8);
            System.out.println("Input is " + BinaryHelpers.toBinaryString(data.getBytes(), 128));
            System.out.println("IV is " + BinaryHelpers.toBinaryString("12345678".getBytes(), 64));
            int[] basesDecimal = {67, 73, 87, 37, 41, 47, 55, 59, 19, 25, 31, 11, 13, 7, 3 };
            int[] keyDecimal = {31, 12, 60, 21, 21, 30, 12, 25, 3, 11, 3, 5,  2,  1, 1 };

            NPNKey fullKey = new NPNKey(basesDecimal, keyDecimal);

            Cipher cipher = new AdilzhanFeistel(fullKey, 128, roundCount, "12345678".getBytes());

            byte[] crypted = cipher.encrypt(data.getBytes());

            byte[] decrypted = cipher.decrypt(crypted);

            String output = "";

            try {
                output = new String(decrypted, "UTF-8");
            } catch(Exception e) {
                System.out.println(e);
            }

            org.junit.Assert.assertEquals("failure - strings are not equal", output, data);
        }
	
    }
    
   @Test
    public void test256bit() {

	String data = "HelloWorHelloWorHelloWorHelloWor";
	int[] rounds = {32};
        for(int roundCount: rounds) {
            System.out.println("Block size is " + data.getBytes().length * 8);
            System.out.println("Input is " + BinaryHelpers.toBinaryString(data.getBytes(), 256));
             System.out.println("IV is " + BinaryHelpers.toBinaryString("1234567835135435".getBytes(), 128));
            int[] basesDecimal = {
                91, //6
                557, // 9
                445, // 8
                545, //9
                117, //6
                529, // 9
                505, // 8
                601, //9
                67, 73, 87, 37, 41, 47, 55, 59, 19, 25, 31, 11, 13, 7, 3 };
            int[] keyDecimal = {92, 111, 63, 70, 45, 49, 73 ,36, 31, 12, 60, 21, 21, 30, 12, 25, 3, 11, 3, 5,  2,  1, 1 };

            NPNKey fullKey = new NPNKey(basesDecimal, keyDecimal);

            Cipher cipher = new AdilzhanFeistel(fullKey, 256, roundCount, "1234567835135435".getBytes());

            byte[] crypted = cipher.encrypt(data.getBytes());

            byte[] decrypted = cipher.decrypt(crypted);

            String output = "";

            try {
                output = new String(decrypted, "UTF-8");
            } catch(Exception e) {
                System.out.println(e);
            }

            org.junit.Assert.assertEquals("failure - strings are not equal", output, data);
        }
	
    }
    
}
