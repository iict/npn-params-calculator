
import org.junit.Test;
import polynomial.BinaryHelpers;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author User
 */
public class BinaryHelpersTest {
    
    @Test
    public void testRotateLeft() {

        
        String input = "01110101";
               
        
        byte[] bytes = BinaryHelpers.fromBinaryString(input);
              
        byte[] shifted = BinaryHelpers.shiftBitsLeft(bytes, -1);

        String output = BinaryHelpers.toBinaryString(shifted, input.length());
        
        org.junit.Assert.assertEquals("failure - strings are not equal", input, output);
    }
}
