/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import org.junit.Test;
import crypto.Cipher;
import crypto.NPNKey;
import crypto.NPNKeyGenerator;
import crypto.SimpleCipher;
import crypto.SimpleFeistel;
import java.io.FileWriter;
import polynomial.BinaryHelpers;


public class SimpleFesitelTest {
    
    @Test
    public void test64bit() {

	String data = "HelloWor";
	
	int[] basesDecimal = {67, 73, 87, 37, 41, 47, 55, 59, 19, 25, 31, 11, 13, 7, 3 };
	int[] keyDecimal = {31, 12, 60, 21, 21, 30, 12, 25, 3, 11, 3, 5,  2,  1, 1 };
	
	NPNKey fullKey = new NPNKey(basesDecimal, keyDecimal);
	
	Cipher cipher = new SimpleFeistel(fullKey, 64);
	
	byte[] crypted = cipher.encrypt(data.getBytes());
	
	byte[] decrypted = cipher.decrypt(crypted);
	
	String output = "";
	
	try {
	    output = new String(decrypted, "UTF-8");
	} catch(Exception e) {
	    System.out.println(e);
	}
	
	org.junit.Assert.assertEquals("failure - strings are not equal", output, data);
	
	
    }
    
    @Test
    public void testKeyGenerator() {

	String data = "HelloWord1234567";
        
        try {
            FileWriter fw = new FileWriter("simpleFeistel.pi");

            for(int i = 0; i < 100; i++) {

                NPNKey fullKey = NPNKeyGenerator.getNPNKey();

                Cipher cipher = new SimpleFeistel(fullKey);

                byte[] crypted = cipher.encrypt(data.getBytes());
                
                fw.write(BinaryHelpers.toBinaryString(crypted, 128) + "\n");

                byte[] decrypted = cipher.decrypt(crypted);

                String output = "";

                try {
                    output = new String(decrypted, "UTF-8");
                } catch(Exception e) {
                    System.out.println(e);
                }

                org.junit.Assert.assertEquals("failure - strings are not equal", data, output);

            }
            
            fw.close();
        }
        catch(Exception e) {
            
        }

    }
    
}
