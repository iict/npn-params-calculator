/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import org.junit.Test;
import crypto.Cipher;
import crypto.NPNKeyGenerator;
import crypto.NPNKey;
import crypto.Feistel;
import crypto.Feistel2;
import crypto.Feistel3;
import crypto.Feistel4;
import crypto.SimpleCipher;
import crypto.SimpleFeistel;
import java.io.FileWriter;
import java.nio.charset.StandardCharsets;
import java.util.List;
import polynomial.BinaryHelpers;
import polynomial.BinaryPolynomial;
import polynomial.BinaryPolynomialMath;
/**
 *
 * @author Мирас
 */
public class FeistelTest {
    
    @Test
    public void test64bit1() {

	String data = "HelloWor";
	
	int[] basesDecimal = {67, 73, 87, 37, 41, 47, 55, 59, 19, 25, 31, 11, 13, 7, 3 };
	int[] keyDecimal = {31, 12, 60, 21, 21, 30, 12, 25, 3, 11, 3, 5,  2,  1, 1 };
	
	NPNKey fullKey = new NPNKey(basesDecimal, keyDecimal);
	
	Cipher cipher = new SimpleFeistel(fullKey, 64);
        
	byte[] crypted = cipher.encrypt(data.getBytes());
	
	byte[] decrypted = cipher.decrypt(crypted);
	
	String output = "";
	
	try {
	    output = new String(decrypted, "UTF-8");
	} catch(Exception e) {
	    System.out.println(e);
	}
       
	org.junit.Assert.assertEquals("failure - strings are not equal", data, output);
	
	
    }
    
     @Test
    public void keysTest() {
        int[] basesDecimal = {113005, 185, 37, 25};
	int[] keyDecimal = {43956, 125, 23, 13};
        int[] data = {23565, 101, 30, 15};
	
	NPNKey fullKey = new NPNKey(basesDecimal, keyDecimal);
        List<BinaryPolynomial> inverse = fullKey.getInverseKey();
        
        for (int i = 0; i < data.length; i++) {
            BinaryPolynomial dt = new BinaryPolynomial(data[i]);
            BinaryPolynomial res1 = BinaryPolynomialMath.mod(BinaryPolynomialMath.multiple(dt, fullKey.getKey().get(i)), fullKey.getBases().get(i));
            BinaryPolynomial res2 = BinaryPolynomialMath.mod(BinaryPolynomialMath.multiple(res1, fullKey.getInverseKey().get(i)), fullKey.getBases().get(i));
            System.out.println("failure - strings are not equal " + dt.toString() + ", " + res2.toString());
            org.junit.Assert.assertEquals("failure - strings are not equal", dt.toString(), res2.toString());
        }
    }   
    
    @Test
    public void test64bit() {

	String data = "HelloWor";
	
	int[] basesDecimal = {
            67, //1000011 6
            59, //111011 5
            19, //10011 4
            25, //11001 4
            31, //11111 4
            11, //1011 3
            13, //1101 3
            7,  //111 2
            3}; //11 1
	int[] keyDecimal = {31, 19, 15, 13, 11, 7, 6, 3, 3, 1 };
	
	NPNKey fullKey = new NPNKey(basesDecimal, keyDecimal);
	
	Cipher cipher = new SimpleFeistel(fullKey, 64);
        
	byte[] crypted = cipher.encrypt(data.getBytes());
	
	byte[] decrypted = cipher.decrypt(crypted);
	
	String output = "";
	
	try {
	    output = new String(decrypted, "UTF-8");
	} catch(Exception e) {
	    System.out.println(e);
	}
	//System.out.println("0 - " + BinaryHelpers.toBinaryString(crypted, crypted.length * 8));
	org.junit.Assert.assertEquals("failure - strings are not equal", data, output);
	
	
    }
    
     @Test
    public void testNested() {

	String data = "HelloWorHelloWor";
	
	int[] basesDecimal = {
            67, //1000011 6
            59, //111011 5
            19, //10011 4
            25, //11001 4
            31, //11111 4
            11, //1011 3
            13, //1101 3
            7,  //111 2
            3}; //11 1
	int[] keyDecimal = {31, 19, 15, 13, 11, 7, 6, 3, 3, 1 };
	
	NPNKey fullKey = new NPNKey(basesDecimal, keyDecimal);
	
	Cipher cipher = new Feistel3(fullKey, 128);
        
	byte[] crypted = cipher.encrypt(data.getBytes());
	
	byte[] decrypted = cipher.decrypt(crypted);
	
	String output = "";
	
	try {
	    output = new String(decrypted, "UTF-8");
	} catch(Exception e) {
	    System.out.println(e);
	}
	//System.out.println("0 - " + BinaryHelpers.toBinaryString(crypted, crypted.length * 8));
	org.junit.Assert.assertEquals("failure - strings are not equal", data, output);
	
	
    }
    
    @Test
    public void test64bit2() {

	String data = "HelloWos";
	
	/*int[] basesDecimal = {67, 73, 87, 37, 41, 47, 55, 59, 19, 25, 31, 11, 13, 7, 3 };
	int[] keyDecimal = {31, 12, 60, 21, 21, 30, 12, 25, 3, 11, 3, 5,  2,  1, 1 };
	*/
        int[] basesDecimal = {
            67, //1000011 6
            59, //111011 5
            19, //10011 4
            25, //11001 4
            31, //11111 4
            11, //1011 3
            13, //1101 3
            7,  //111 2
            3}; //11 1
	int[] keyDecimal = {31, 19, 15, 13, 11, 7, 6, 3, 3, 1 };
	NPNKey fullKey = new NPNKey(basesDecimal, keyDecimal);
	
	Cipher cipher = new SimpleCipher(fullKey, 64);
        byte[] bytes = {2,0,0,0,0,0,0,0};
	byte[] crypted = cipher.encrypt(bytes);
	
	byte[] decrypted = cipher.decrypt(crypted);
	
	String output = "";
	
	try {
	    output = new String(decrypted, "UTF-8");
	} catch(Exception e) {
	    System.out.println(e);
	}
	System.out.println("1 - " + BinaryHelpers.toBinaryString(crypted, crypted.length * 8));
	org.junit.Assert.assertEquals("failure - strings are not equal", data, output);
	
	
    }
    
     @Test
    public void test64bit3() {

        byte[] bytes = {0,0,0,0,0,0,0,0};
	//String data = new String(bytes, StandardCharsets.UTF_8);
	String data = "HelloWos";
	/*int[] basesDecimal = {67, 73, 87, 37, 41, 47, 55, 59, 19, 25, 31, 11, 13, 7, 3 };
	int[] keyDecimal = {31, 12, 60, 21, 21, 30, 12, 25, 3, 11, 3, 5,  2,  1, 1 };
	*/
        int[] basesDecimal = {
            67, //1000011 6
            59, //111011 5
            19, //10011 4
            25, //11001 4
            31, //11111 4
            11, //1011 3
            13, //1101 3
            7,  //111 2
            3}; //11 1
	int[] keyDecimal = {31, 19, 15, 13, 11, 7, 6, 3, 3, 1 };
        //int[] keyDecimal = {0,0,0,0,0,0,0,0,0,0};
	NPNKey fullKey = new NPNKey(basesDecimal, keyDecimal);
	
	Cipher cipher = new Feistel4(fullKey, 64);
        
	byte[] crypted = cipher.encrypt(data.getBytes());
	
	byte[] decrypted = cipher.decrypt(crypted);
	
	String output = "";
	
	try {
	    output = new String(decrypted, "UTF-8");
	} catch(Exception e) {
	    System.out.println(e);
	}
	System.out.println("2 - " + BinaryHelpers.toBinaryString(crypted, crypted.length * 8));
	org.junit.Assert.assertEquals("failure - strings are not equal", data, output);

	//0100100001100101011011000110110001101111010101110110111101110011
    }
    
    @Test
    public void test128bit() {

	String data = "HelloWorHelloWor";
	
	int[] basesDecimal = {
            91, //6
            557, // 9
            445, // 8
            545, //9
            117, //6
            529, // 9
            505, // 8
            601, //9
            67, 73, 87, 37, 41, 47, 55, 59, 19, 25, 31, 11, 13, 7, 3 };
	int[] keyDecimal = {92, 111, 63, 70, 45, 49, 73 ,36, 31, 12, 60, 21, 21, 30, 12, 25, 3, 11, 3, 5,  2,  1, 1 };
	
	NPNKey fullKey = NPNKeyGenerator.getNPNKey();
	
	Cipher cipher = new SimpleFeistel(fullKey, 128);
        
	byte[] crypted = cipher.encrypt(data.getBytes());
	
	byte[] decrypted = cipher.decrypt(crypted);
	
	String output = "";
	
	try {
	    output = new String(decrypted, "UTF-8");
	} catch(Exception e) {
	    System.out.println(e);
	}
	
	org.junit.Assert.assertEquals("failure - strings are not equal", data, output);
	
	
    }
    
    
    
    //@Test
    public void testKeyGenerator() {

	String data = "HelloWord1234567";
        
        try {
            FileWriter fw = new FileWriter("simpleCipher.pi");

            for(int i = 0; i < 100; i++) {

                NPNKey fullKey = NPNKeyGenerator.getNPNKey();

                Cipher cipher = new Feistel(fullKey);

                byte[] crypted = cipher.encrypt(data.getBytes());
                
                fw.write(BinaryHelpers.toBinaryString(crypted, 128) + "\n");

                byte[] decrypted = cipher.decrypt(crypted);

                String output = "";

                try {
                    output = new String(decrypted, "UTF-8");
                } catch(Exception e) {
                    System.out.println(e);
                }

                org.junit.Assert.assertEquals("failure - strings are not equal", data, output);

            }
            
            fw.close();
        }
        catch(Exception e) {
            
        }

    }
    
}
