/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import org.junit.Test;
import crypto.Cipher;
import crypto.NPNKey;
import crypto.SimpleCipher;
import polynomial.BinaryPolynomial;
import polynomial.BinaryPolynomialMath;
/**
 *
 * @author Мирас
 */
public class PolynomialMathTest {
    
    @Test
    public void testAdd() {

    }
    
    @Test
    public void testSubstract() {

    }
    
    @Test
    public void testMultiple() {

    }
    
    @Test
    public void testEuclid() {
        BinaryPolynomial base = new BinaryPolynomial("111011");
        BinaryPolynomial res = BinaryPolynomialMath.ExtendedEuclid(new BinaryPolynomial("11101"), base)[1];
        System.out.println(res.toFullString(base.getDegree()));
    }
}
