**Приложение для расчета параметров полиномиальной НПС.** 

Разрабатывается в рамках проекта грантового финансирования Г.2018 AP05132469 "РАЗРАБОТКА ПРОГРАММНО-АППАРАТНЫХ СРЕДСТВ ДЛЯ КРИПТОСИСТЕМ НА БАЗЕ НЕПОЗИЦИОННОЙ СИСТЕМЫ СЧИСЛЕНИЯ" выполняемого в ИИВТ КН МОН РК.

Работа криптосистемы на основе полиномиальной НПСС предусматривает выполнение предварительных расчётов параметров непозиционной полиномиальной системы. Данные расчёты включают в себя следующие шаги:
–	создание базы данных неприводимых полиномов с коэффициентами над GF(2);
–	определение необходимой длины N блока и ключа шифрования;
–	формирование в соответствии с длиной блока системы оснований. Для формирования полиномиальной НПСС при обработке блока длиной N бит из множества всех неприводимых многочленов степени не выше значения N выбираются рабочие основания.
–	подбор и проверка содержания системы оснований. Все выбираемые основания должны отличаться друг от друга, даже если они являются неприводимыми полиномами одной степени. Тогда в этой системе любой многочлен степени меньше суммы степеней всех выбранных рабочих оснований имеет единственное представление в виде последовательности остатков (вычетов) от деления его на данные основания.
–	предварительная интерпретация входного блока данных в виде остатков от деления на выбранную систему оснований. При этом сгенерированная ключевая последовательность так же может быть представлена в виде последовательностей вычетов, а для расшифрования необходимо вычислить обратный многочлен к каждому из этих вычетов по модулю соответствующего основания.


**Tool for calculation parameters of polynomial RNS. It is developed in frame of the grant funding project AP05132469 in IICT SC MES of RK.**

The operation of a cryptosystem based on a polynomial NPN provides for preliminary calculations of the parameters of a non-positional polynomial system. These calculations include the following steps:
–	creation of a database of irreducible polynomials with coefficients over GF (2)
–	determining the required length N of data block and encryption key;
–	formation in accordance with the block length of the base system. For the formation of a polynomial NPN when processing a block of length N bits from the set of all irreducible polynomials of degree not higher than the value of N, working bases are selected.
–	selection and verification of the content of the basis system. All selected bases must be different from each other, even if they are irreducible polynomials of the same degree. Then, in this system, any polynomial of degree less than the sum of the degrees of all the selected working bases has a unique representation in the form of a sequence of residues from dividing it by the given bases.
–	preliminary interpretation of the input data block in the form of residuals from division by the selected base system. In this case, the generated key sequence can also be represented as sequences of residues, and for decryption it is necessary to calculate the inverse polynomial to each of these residues modulo the corresponding base.


