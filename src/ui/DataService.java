/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author Miras
 */
public class DataService {
    
    private  Connection conn = null;
    
    public DataService() {
       this.connect();
       this.createBasesTable();
    }
    
    private void connect() {
      
       try {
           File dbfile=new File(".");
           // db parameters
           String url = "jdbc:sqlite:crypto.db";
           // create a connection to the database
           conn = DriverManager.getConnection(url);

           System.out.println("Connection to SQLite has been established.");

       } catch (SQLException e) {
           System.out.println(e.getMessage());
       }
   }
    
    public void createBasesTable() {      
        // SQL statement for creating a new table
        String sql = "CREATE TABLE IF NOT EXISTS bases (\n"
                + "	id integer PRIMARY KEY,\n"
                + "	power integer NOT NULL,\n"
                + "	value text NOT NULL\n"
                + ");";
        
        try (Statement stmt = conn.createStatement()) {
            // create a new table
            stmt.execute(sql);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    public List<Base> selectAllBases(){
        String sql = "SELECT id, power, value FROM bases";
        
        try (Statement stmt  = conn.createStatement();
            ResultSet rs    = stmt.executeQuery(sql)){
            
            List<Base> result = new ArrayList();
            // loop through the result set
            while (rs.next()) {
                result.add(new Base(rs.getInt("id"), rs.getInt("power"), rs.getString("value")));
            }
            return result;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return null;
        }
    }
    
    public List<Base> selectBasesByOrder(int order){
        String sql = "SELECT id, power, value FROM bases WHERE power = " + order;
       
        try (Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql)){
            List<Base> result = new ArrayList();
            // loop through the result set
            while (rs.next()) {
                result.add(new Base(rs.getInt("id"), rs.getInt("power"), rs.getString("value")));
            }
            return result;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return null;
        }
    }
}
