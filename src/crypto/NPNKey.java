/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crypto;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import polynomial.BinaryPolynomial;
import polynomial.BinaryPolynomialMath;


public class NPNKey {
    
    protected List<BinaryPolynomial> key = new ArrayList();
    protected List<BinaryPolynomial> inverseKey = new ArrayList();
    protected List<BinaryPolynomial> bases = new ArrayList();
    
    protected int[] basesDecimal;
    protected int[] keyDecimal;
   
    public NPNKey(int[] basesDecimal, int[] keyDecimal) {
	
	//TODO: check input data validity!
	
	this.basesDecimal = basesDecimal;
	this.keyDecimal = keyDecimal;
	
	for(int i = 0; i < basesDecimal.length; i++) {
	    
	    BinaryPolynomial base = new BinaryPolynomial(basesDecimal[i]);
	    BinaryPolynomial k = new BinaryPolynomial(keyDecimal[i]);
	    BinaryPolynomial poly = BinaryPolynomialMath.ExtendedEuclid(k, base)[1];
	    
	    bases.add(base);
	    key.add(k);
	    inverseKey.add(poly);

	}
	
    }
    
    public NPNKey(List<BinaryPolynomial> bases, List<BinaryPolynomial> key) {
	
	//TODO: check input data validity!
	
        this.basesDecimal = new int[bases.size()];
        this.keyDecimal = new int[bases.size()];
        
        this.bases = bases;
        this.key = key;
        
	for(int i = 0; i < bases.size(); i++) {
	    
            this.keyDecimal[i] = key.get(i).toInt();

            this.basesDecimal[i] = bases.get(i).toInt();
                    
	    BinaryPolynomial poly = BinaryPolynomialMath.ExtendedEuclid(key.get(i), bases.get(i))[1];

	    inverseKey.add(poly);

	}
	
    }
    
    public int[] getKeyDecimal() {
	return this.keyDecimal;
    }
    
    public int[] getBasesDecimal() {
	return this.basesDecimal;
    }
    
    public List<BinaryPolynomial> getKey() {
	return key;
    }

    public List<BinaryPolynomial> getBases() {
	return bases;
    }
    
    public List<BinaryPolynomial> getInverseKey() {
	return inverseKey;
    }
    
}
