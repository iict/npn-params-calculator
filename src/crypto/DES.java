/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crypto;

import static crypto.Feistel4.MODE_ENCRYPT;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.Security;
import java.util.Arrays;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

/**
 *
 * @author User
 */
public class DES implements crypto.Cipher {

    
    static int MODE_ENCRYPT = 0;
    static int MODE_DECRYPT = 1;
    

    @Override
    public byte[] encrypt(byte[] input) {
        try {
            return desEncrypt(input, "E1BB465D57CAE7ACDBBE8091F9CE83DF");
        } catch(Exception e) {
            System.out.println(e);
            return null;
        }
    }

    @Override
    public byte[] decrypt(byte[] input) {
         try {
            return desDecrypt(input, "E1BB465D57CAE7ACDBBE8091F9CE83DF");
        } catch(Exception e) {
            System.out.println(e);
            return null;
        }

    }
    public static byte[] desEncrypt(byte[] plaintext, String keyStr) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, IOException, InvalidAlgorithmParameterException {
        final MessageDigest md = MessageDigest.getInstance("md5");
        final byte[] digestOfPassword = md.digest(keyStr
                .getBytes("utf-8"));
        final byte[] keyBytes = Arrays.copyOf(digestOfPassword, 24);
        for (int j = 0, k = 16; j < 8;) {
            keyBytes[k++] = keyBytes[j++];
        }

        final SecretKey key = new SecretKeySpec(keyBytes, "DESede");
        final IvParameterSpec iv = new IvParameterSpec(new byte[8]);
        final Cipher cipher = Cipher.getInstance("DESede/CBC/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, key, iv);
        return cipher.doFinal(plaintext);
    }

    public static byte[] desDecrypt(byte[] encryptedData, String keyStr) throws InvalidKeyException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException {
        final MessageDigest md = MessageDigest.getInstance("md5");
        final byte[] digestOfPassword = md.digest(keyStr
                .getBytes("utf-8"));
        final byte[] keyBytes = Arrays.copyOf(digestOfPassword, 24);
        for (int j = 0, k = 16; j < 8;) {
            keyBytes[k++] = keyBytes[j++];
        }

        final SecretKey key = new SecretKeySpec(keyBytes, "DESede");
        final IvParameterSpec iv = new IvParameterSpec(new byte[8]);
        final Cipher decipher = Cipher.getInstance("DESede/CBC/PKCS5Padding");
        decipher.init(Cipher.DECRYPT_MODE, key, iv);

        // final byte[] encData = new
        // sun.misc.BASE64Decoder().decodeBuffer(message);
        return  decipher.doFinal(encryptedData);

    }
    
     public static void testDES(String filename) {
        
            BufferedInputStream bis = null, dbis = null;
            BufferedOutputStream bos = null, dbos = null;
             try {
                    //long startTime = System.currentTimeMillis();
       
                    
                    bis = new BufferedInputStream(new FileInputStream(
                            "bigtest/" + filename));
                    bos = new BufferedOutputStream(new FileOutputStream(
                            "bigtest/crypted/miras_" + filename));

                    dbis = new BufferedInputStream(new FileInputStream(
                            "bigtest/crypted/miras_" + filename));
                    dbos = new BufferedOutputStream(new FileOutputStream(
                            "bigtest/decrypted/" + filename));

                    byte[] block = new byte[8];

                    crypto.Cipher cipher = new DES();

                    byte[] prev = new byte[8];

                    byte[] init = {5, 12, 4, 2, 18, 33, 51, 71};

                    boolean first = false;
                    long startTime = System.currentTimeMillis();
                    while (bis.read(block) != -1) {
                        
                        //System.out.println("======= BLOCK =========");
                        //System.out.println(new String(block, "cp1251"));
                        byte[] c = cipher.encrypt(block);
           
                        bos.write(c);

                        block = new byte[8];
                    }

                    bos.flush();
                    long endTime = System.currentTimeMillis();
                     System.out.println("DES Total execution time: " + (endTime-startTime) + "ms"); 

                    /*first = false;
                    block = new byte[32];
                    while (dbis.read(block) != -1) {

                        byte[] d = cipher.decrypt(block);
                        dbos.write(d);
               
                        block = new byte[32];
                    }*/

                    if (bis != null) {
                        bis.close();
                    }
                    if (dbis != null) {
                        dbis.close();
                    }
                    if (bos != null) {
                        bos.close();
                    }
                    if (dbos != null) {
                        dbos.close();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
   }
}
