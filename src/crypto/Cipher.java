/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crypto;


public interface Cipher {
    public byte[] encrypt(byte[] input);

    public byte[] decrypt(byte[] input);
}
