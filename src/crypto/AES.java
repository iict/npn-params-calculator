/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crypto;

import static crypto.Feistel4.MODE_ENCRYPT;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

/**
 *
 * @author User
 */
public class AES implements crypto.Cipher {

    private static final String ALGORITMO = "AES/CBC/PKCS5Padding";
    private static final String CODIFICACION = "UTF-8";
    
    static int MODE_ENCRYPT = 0;
    static int MODE_DECRYPT = 1;

    @Override
    public byte[] encrypt(byte[] input) {
        try {
            return aesEncrypt(input, "E1BB465D57CAE7ACDBBE8091F9CE83DF");
        } catch(Exception e) {
            System.out.println(e);
            return null;
        }
    }

    @Override
    public byte[] decrypt(byte[] input) {
         try {
            return aesDecrypt(input, "E1BB465D57CAE7ACDBBE8091F9CE83DF");
        } catch(Exception e) {
            System.out.println(e);
            return null;
        }

    }
    public static byte[] aesEncrypt(byte[] plaintext, String key) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, IOException {
        byte[] raw = DatatypeConverter.parseHexBinary(key);
        SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
        Cipher cipher = Cipher.getInstance(ALGORITMO);
        cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
        return cipher.doFinal(plaintext);
    }

    public static byte[] aesDecrypt(byte[] encryptedData, String key) throws InvalidKeyException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException {
        byte[] raw = DatatypeConverter.parseHexBinary(key);
        SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
        Cipher cipher = Cipher.getInstance(ALGORITMO);
        byte[] iv = Arrays.copyOfRange(encryptedData, 0, 16);
        byte[] cipherText = Arrays.copyOfRange(encryptedData, 16, encryptedData.length);
        IvParameterSpec iv_specs = new IvParameterSpec(iv);
	cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv_specs);
        return cipher.doFinal(cipherText);
    }
    
    public static void testAES(String filename) {
        
            BufferedInputStream bis = null, dbis = null;
            BufferedOutputStream bos = null, dbos = null;
             try {
                    //long startTime = System.currentTimeMillis();
       
                   
                   
                    bis = new BufferedInputStream(new FileInputStream(
                            "bigtest/" + filename));
                    bos = new BufferedOutputStream(new FileOutputStream(
                            "bigtest/crypted/miras_" + filename));

                    dbis = new BufferedInputStream(new FileInputStream(
                            "bigtest/crypted/miras_" + filename));
                    dbos = new BufferedOutputStream(new FileOutputStream(
                            "bigtest/decrypted/" + filename));

                    byte[] block = new byte[16];

                    crypto.Cipher cipher = new AES();

                    byte[] prev = new byte[16];

                    byte[] init = {5, 12, 4, 2, 18, 33, 51, 71, 7, 12, 11, 0, 52, 35, 58, 1};

                    boolean first = false;
                    long startTime = System.currentTimeMillis();
                    while (bis.read(block) != -1) {
                        
                        //System.out.println("======= BLOCK =========");
                        //System.out.println(new String(block, "cp1251"));
                        byte[] c = cipher.encrypt(block);
           
                        bos.write(c);

                        block = new byte[16];
                    }

                    bos.flush();
                    long endTime = System.currentTimeMillis();
                     System.out.println("AES Total execution time: " + (endTime-startTime) + "ms"); 

                    /*first = false;
                    block = new byte[32];
                    while (dbis.read(block) != -1) {

                        byte[] d = cipher.decrypt(block);
                        dbos.write(d);
               
                        block = new byte[32];
                    }*/

                    if (bis != null) {
                        bis.close();
                    }
                    if (dbis != null) {
                        dbis.close();
                    }
                    if (bos != null) {
                        bos.close();
                    }
                    if (dbos != null) {
                        dbos.close();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
   }
}
