/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crypto;

import java.util.ArrayList;
import java.util.List;
import polynomial.BinaryHelpers;
import polynomial.BinaryPolynomial;
import polynomial.BinaryPolynomialMath;

public class Feistel2 implements Cipher {

    static int MODE_ENCRYPT = 0;
    static int MODE_DECRYPT = 1;

    protected NPNKey fullKey;

    protected int blockSize = 128;

    public Feistel2(NPNKey fullKey) {
        this.fullKey = fullKey;
    }

    public Feistel2(NPNKey fullKey, int blockSize) {
        this.fullKey = fullKey;
        this.blockSize = blockSize;
    }

    @Override
    public byte[] encrypt(byte[] input) {
        return this.process(input, MODE_ENCRYPT);
    }

    @Override
    public byte[] decrypt(byte[] input) {
        return this.process(input, MODE_DECRYPT);
    }

    public int getBlockSize() {
        return blockSize;
    }

    public void setBlockSize(int blockSize) {
        this.blockSize = blockSize;
    }

    protected byte[] process(byte[] input, int mode) {

        String bitStr = BinaryHelpers.toBinaryString(input, this.blockSize);
        
        for (int j = 0; j < 4; j++) {
            List<String> resultArr = new ArrayList<>();
            int keyIndex = 0;
            int prevIndex = 0;
            if (j > 0) {
                int half = bitStr.length() / 2;
                bitStr = bitStr.substring(half) + bitStr.substring(0, half);
            }
            //System.out.println(bitStr);
            
            // omp parallel for
            for (BinaryPolynomial base : fullKey.getBases()) {

                BinaryPolynomial dt = new BinaryPolynomial(bitStr.substring(prevIndex, prevIndex + base.getDegree()));

                prevIndex = prevIndex + base.getDegree();

                BinaryPolynomial result;

                if (mode == MODE_ENCRYPT) {

                    result = BinaryPolynomialMath.mod(BinaryPolynomialMath.multiple(dt, fullKey.getKey().get(keyIndex)), base);

                } else {

                    result = BinaryPolynomialMath.mod(BinaryPolynomialMath.multiple(dt, fullKey.getInverseKey().get(keyIndex)), base);

                }

                resultArr.add(result.toFullString(base.getDegree()));

                keyIndex++;

            }
           
            bitStr = String.join("", resultArr);
        }
        return BinaryHelpers.fromBinaryString(bitStr);
    }

    protected byte[] concat(byte[] a, byte[] b) {
        int aLen = a.length;
        int bLen = b.length;
        byte[] c = new byte[aLen + bLen];
        System.arraycopy(a, 0, c, 0, aLen);
        System.arraycopy(b, 0, c, aLen, bLen);
        return c;
    }

}
