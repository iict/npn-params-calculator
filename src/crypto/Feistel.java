/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crypto;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import polynomial.BinaryHelpers;
import polynomial.BinaryPolynomial;
import polynomial.BinaryPolynomialMath;

public class Feistel implements Cipher {

    static int MODE_ENCRYPT = 0;
    static int MODE_DECRYPT = 1;
    
    protected NPNKey fullKey;

    protected int blockSize = 128;
    
    public Feistel(NPNKey fullKey) {
	this.fullKey = fullKey;    
    }

    public Feistel(NPNKey fullKey, int blockSize) {
	this.fullKey = fullKey;    
        this.blockSize = blockSize;
    }
     
    @Override
    public byte[] encrypt(byte[] input) {
	return this.process(input, MODE_ENCRYPT);
    }

    @Override
    public byte[] decrypt(byte[] input) {
	return this.process(input, MODE_DECRYPT);
    }
    
    public int getBlockSize() {
        return blockSize;
    }

    public void setBlockSize(int blockSize) {
        this.blockSize = blockSize;
    }
    
    protected byte[] process(byte[] input, int mode) {
	int keyIndex = 0;
	int prevIndex = 0;

	String resultArr = "";

        String bitStr = BinaryHelpers.toBinaryString(input, this.blockSize);

        System.out.println("Input: " + bitStr);
        List<BinaryPolynomial> currentList = new ArrayList();

        for (BinaryPolynomial base : fullKey.getBases()) {

            BinaryPolynomial dt = new BinaryPolynomial(bitStr.substring(prevIndex, prevIndex + base.getDegree()));

            prevIndex = prevIndex + base.getDegree();

            BinaryPolynomial result;

            if(mode == MODE_ENCRYPT) {

                result = BinaryPolynomialMath.mod(BinaryPolynomialMath.multiple(dt, fullKey.getKey().get(keyIndex)), base);

            } else {

                result = BinaryPolynomialMath.mod(BinaryPolynomialMath.multiple(dt, fullKey.getInverseKey().get(keyIndex)), base);

            }

            currentList.add(result);

            keyIndex++;
        } 
        int j = 0;
        for (BinaryPolynomial res: currentList) {
            resultArr += res.toFullString(fullKey.getBases().get(j).getDegree());
            j++;
        }
        System.out.println("Before feistel: " + resultArr);
        System.out.println("Before list: " + currentList);
      
            if(mode == MODE_ENCRYPT) {
                Collections.rotate(currentList, Math.round(currentList.size() / 2));
            } else {
                Collections.rotate(currentList, -Math.round(currentList.size() / 2));
            }
            System.out.println("After rotate list: " + currentList);
            for (ListIterator<BinaryPolynomial> iterator = currentList.listIterator(); iterator.hasNext();) {
                BinaryPolynomial dt = iterator.next();
                BinaryPolynomial result;
                if(mode == MODE_ENCRYPT) {

                    result = BinaryPolynomialMath.mod(BinaryPolynomialMath.multiple(dt, fullKey.getKey().get(iterator.nextIndex() - 1)), fullKey.getBases().get(iterator.nextIndex() - 1));

                } else {

                    result = BinaryPolynomialMath.mod(BinaryPolynomialMath.multiple(dt, fullKey.getInverseKey().get(iterator.nextIndex() - 1)), fullKey.getBases().get(iterator.nextIndex() - 1));

                }
                iterator.set(result);
            }
         if(mode == MODE_ENCRYPT) {
                Collections.rotate(currentList, -Math.round(currentList.size() / 2));
            } else {
                Collections.rotate(currentList, Math.round(currentList.size() / 2));
            }
        
        System.out.println("After all list: " + currentList);

        int i = 0;
        resultArr = "";
        for (BinaryPolynomial res: currentList) {
            resultArr += res.toFullString(fullKey.getBases().get(i).getDegree());
            i++;
        }
	return BinaryHelpers.fromBinaryString(resultArr);
    }

    protected byte[] concat(byte[] a, byte[] b) {
	int aLen = a.length;
	int bLen = b.length;
	byte[] c = new byte[aLen + bLen];
	System.arraycopy(a, 0, c, 0, aLen);
	System.arraycopy(b, 0, c, aLen, bLen);
	return c;
    }

  
}
