/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crypto;

import static crypto.SimpleCipher.MODE_ENCRYPT;
import java.util.Arrays;
import polynomial.BinaryHelpers;
import polynomial.BinaryPolynomial;
import polynomial.BinaryPolynomialMath;

/**
 *
 * @author User
 */
public class SimpleFeistel implements Cipher {
    
    static int MODE_ENCRYPT = 0;
    static int MODE_DECRYPT = 1;
    
    protected int roundCount = 4;
    
    protected int blockSize = 128;
   
    protected NPNKey fullKey;
    
    protected SimpleCipher npnCipher;

    protected byte[] key;
    protected byte[] bases;
    
    public SimpleFeistel(NPNKey fullKey) {
	this.fullKey = fullKey;  
        
        this.npnCipher = new SimpleCipher(fullKey, this.blockSize);
        
        this.key = BinaryHelpers.intArrayToByteArray(fullKey.getKeyDecimal());
        this.bases =  BinaryHelpers.intArrayToByteArray(fullKey.getBasesDecimal());
    }
    
    public SimpleFeistel(NPNKey fullKey, int blockSize) {
	this.fullKey = fullKey;  
        
        this.blockSize = blockSize;
        
        this.npnCipher = new SimpleCipher(fullKey, this.blockSize);
        
        this.key = BinaryHelpers.intArrayToByteArray(fullKey.getKeyDecimal());
        
    }
    
    @Override
    public byte[] encrypt(byte[] input) {
        //System.out.println(this.npnCipher.encrypt(this.process(input, MODE_ENCRYPT)).length);
	return this.npnCipher.encrypt(this.process(input, MODE_ENCRYPT));
    }

    @Override
    public byte[] decrypt(byte[] input) {
        return this.process(this.npnCipher.decrypt(input), MODE_DECRYPT);

    }

     public int getBlockSize() {
        return blockSize;
    }

    public void setBlockSize(int blockSize) {
        this.blockSize = blockSize;
    }
    
    protected byte[] process(byte[] input, int mode) {

        byte[] left = new byte[input.length / 2];
        byte[] right = new byte[input.length / 2];
        
        byte[][] roundKeys = new byte[roundCount][];

        for(int i = 0; i < roundCount; i++ ) {
            roundKeys[i] = getRoundKey(i);
           // System.out.println(BinaryHelpers.toBinaryString(roundKeys[i], 32));
        }

        System.arraycopy(input, 0, left, 0, left.length);
        System.arraycopy(input, left.length, right, 0, right.length);
        
        byte[] roundKey;
                
        for(int i = 0; i < roundCount; i++ ) {
            
            if(mode == MODE_ENCRYPT) {
                roundKey = roundKeys[i];
            } else {
                roundKey = roundKeys[(roundCount - 1) - i];
            }
    
            byte[] temp = f(left, roundKey);
           
            for(int j = 0; j < temp.length; j++) {
                temp[j] = (byte)(temp[j] ^ right[j]);
            }
            
            right = left;
            left = temp;
        }
        
        return BinaryHelpers.concat(right, left);
    }
    
    protected byte[] f(byte[] subblock, byte[] key) {
        
        byte[] result = new byte[subblock.length];
        
        for(int i = 0; i < subblock.length; i++) {
            result[i] = (byte)(subblock[i] ^ key[i]);
        }
        
        return result;
    }
    
    protected byte[] getRoundKey(int roundNum) {
        return Arrays.copyOfRange(BinaryHelpers.shiftBitsLeft(key, (roundNum + 1) * 2), 0, this.blockSize / 2);
    }
    
    protected static byte[] shuffle(byte[] a) {
        int aLen = a.length;
        int middle = aLen / 2;
        byte[] c = new byte[aLen];
        System.arraycopy(a, 0, c, middle, middle);
        System.arraycopy(a, middle, c, 0, middle);
        return c;
    }
    
}
