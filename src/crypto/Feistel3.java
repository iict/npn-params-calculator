/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crypto;

import static crypto.SimpleCipher.MODE_ENCRYPT;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Arrays;
import java.util.BitSet;
import java.util.stream.IntStream;
import polynomial.BinaryHelpers;
import polynomial.BinaryPolynomial;
import polynomial.BinaryPolynomialMath;

/**
 *
 * @author User
 */
public class Feistel3 implements Cipher {
    
    static int MODE_ENCRYPT = 0;
    static int MODE_DECRYPT = 1;
    
    protected int topRoundCount = 4;
    
    protected int roundCount = 4;
    
    protected int blockSize = 128;
   
    protected NPNKey fullKey;
    
    protected SimpleCipher npnCipher;

    protected byte[] key;
    protected byte[] bases;
    protected byte[][] roundKeys = new byte[roundCount][];
    
    public Feistel3(NPNKey fullKey) {
	this.fullKey = fullKey;  
        
        this.npnCipher = new SimpleCipher(fullKey, this.blockSize);
        
        this.key = BinaryHelpers.intArrayToByteArray(fullKey.getKeyDecimal());
        this.bases =  BinaryHelpers.intArrayToByteArray(fullKey.getBasesDecimal());
    }
    
    public Feistel3(NPNKey fullKey, int blockSize) {
	this.fullKey = fullKey;  
        
        this.blockSize = blockSize;
        
        this.npnCipher = new SimpleCipher(fullKey, this.blockSize / 4);
        
        this.key = BinaryHelpers.intArrayToByteArray(fullKey.getKeyDecimal());
        
        for(int i = 0; i < topRoundCount; i++ ) {
            roundKeys[i] = getRoundKey(i);
        }
    }
    
    @Override
    public byte[] encrypt(byte[] input) {
	return this.process(input, MODE_ENCRYPT);
    }

    @Override
    public byte[] decrypt(byte[] input) {
        return this.process(input, MODE_DECRYPT);

    }

     public int getBlockSize() {
        return blockSize;
    }

    public void setBlockSize(int blockSize) {
        this.blockSize = blockSize;
    }
    
    protected byte[] process(byte[] input, int mode) {
        //System.out.println("MAIN KEY  " + BinaryHelpers.toBinaryString(key, 64));
        byte[] left = new byte[input.length / 2];
        byte[] right = new byte[input.length / 2];
        
       
        System.arraycopy(input, 0, left, 0, left.length);
        System.arraycopy(input, left.length, right, 0, right.length);
        
        byte[] roundKey;
                
        for(int i = 0; i < topRoundCount; i++ ) {
            
            if(mode == MODE_ENCRYPT) {
                roundKey = roundKeys[i];
            } else {
                roundKey = roundKeys[(topRoundCount - 1) - i];
            }
    
            final byte[] temp = f(left, roundKey);
                       
            for(int j = 0; j < temp.length; j++) {
                temp[j] = (byte)(temp[j] ^ right[j]);
            }
            
            right = left;
            left = temp;
            
        }
        
        return BinaryHelpers.concat(right, left);
    }
    
    protected byte[] f(byte[] subblock, byte[] key) {
        
      
        byte[] left = new byte[subblock.length / 2];
        byte[] right = new byte[subblock.length / 2];
        
        System.arraycopy(subblock, 0, left, 0, left.length);
        System.arraycopy(subblock, left.length, right, 0, right.length);
                
        for(int i = 0; i < roundCount; i++ ) {   
            final byte[] temp = this.npnCipher.encrypt(left);
                     
            for(int j = 0; j < temp.length; j++) {
                temp[j] = (byte)(temp[j] ^ right[j]);
            }
            
            right = left;
            left = temp; 
        }
        byte[] result = BinaryHelpers.concat(right, left);;
        for(int j = 0; j < result.length; j++) {
            result[j] = (byte)(result[j] ^ key[j]);
        }
        return result;
   
    }
    
    protected byte[] getRoundKey(int roundNum) {
        BinaryPolynomial p = this.fullKey.bases.get(roundNum % this.fullKey.bases.size());
        byte[] keys = Arrays.copyOfRange(BinaryHelpers.shiftBitsLeft(this.key, (roundNum * p.getDegree())), 0, this.blockSize / 2);
        for(int j = 0; j < keys.length; j++) {
                int val = keys[j] < 0 ? keys[j] & 0xFF: keys[j];
                keys[j] = (byte)this.sblock((val + j * roundNum * p.getDegree()) % 255);
            }
        //System.out.println("KEY of " + roundNum + " " + BinaryHelpers.toBinaryString(keys, 32));
        return keys;
    }
    
    protected static byte[] shuffle(byte[] a) {
        int aLen = a.length;
        int middle = aLen / 2;
        byte[] c = new byte[aLen];
        System.arraycopy(a, 0, c, middle, middle);
        System.arraycopy(a, middle, c, 0, middle);
        return c;
    }
    
    private short[] s = {37,	127,	46,	108,	23,	56,	96,	51,	141,	33,	120,	84,	75,	5,	103,	69,

151,	9,	111,	185,	137,	146,	15,	171,	115,	191,	41,	181,	123,	132,	62,	6,

85,	101,	174,	27,	73,	226,	65,	211,	20,	149,	223,	36,	95,	113,	188,	39,

32,	164,	77,	202,	121,	144,	161,	91,	155,	60,	126,	153,	175,	24,	167,	0,

117,	180,	218,	43,	183,	12,	233,	204,	30,	213,	178,	52,	136,	162,	99,	159,

70,	195,	22,	237,	81,	220,	48,	107,	229,	87,	2,	109,	255,	68,	200,	17,

173,	40,	187,	55,	114,	245,	130,	241,	94,	209,	234,	193,	57,	197,	47,	147,

90,	232,	214,	243,	217,	61,	236,	1,	166,	35,	134,	216,	3,	118,	143,	72,

157,	7,	59,	206,	247,	254,	64,	252,	170,	199,	239,	14,	222,	190,	88,	179,

76,	168,	249,	160,	13,	251,	230,	242,	253,	71,	250,	246,	102,	19,	224,	129,

208,	26,	145,	98,	248,	44,	227,	125,	11,	156,	92,	231,	58,	212,	139,	78,

140,	54,	198,	221,	244,	201,	215,	240,	219,	238,	53,	203,	186,	110,	31,	172,

18,	225,	165,	86,	34,	235,	104,	29,	176,	112,	228,	8,	131,	207,	194,	25,

133,	189,	49,	124,	192,	119,	210,	205,	63,	82,	182,	38,	150,	42,	163,	122,

105,	4,	177,	184,	66,	196,	16,	154,	93,	106,	74,	142,	135,	158,	97,	10,

67,	148,	89,	28,	152,	80,	138,	100,	50,	169,	21,	128,	83,	45,	116,    79
};
    
    protected int sblock(int in) {
        //System.out.println("S " + in);
        return s[in];
    }
    
    public static void testNPNs(String filename) {
        
            BufferedInputStream bis = null, dbis = null;
            BufferedOutputStream bos = null, dbos = null;
               try {
                    bis = new BufferedInputStream(new FileInputStream(
                            "bigtest/" + filename));
                    bos = new BufferedOutputStream(new FileOutputStream(
                            "bigtest/crypted/miras_1" + filename));

                    dbis = new BufferedInputStream(new FileInputStream(
                            "bigtest/crypted/miras_" + filename));
                    dbos = new BufferedOutputStream(new FileOutputStream(
                            "bigtest/decrypted/" + filename));

                    byte[] block = new byte[8];

                    int[] basesDecimal = {
                      //  67, //100101 6
                      //  59, //111011 5
                       // 19, //10011 4
                        25, //11001 4
                        31, //11111 4
                        11, //1011 3
                        13, //1101 3
                        7,  //111 2
                        //3
                    }; //11 1
                    int[] keyDecimal = {13, 11, 7, 6, 3};
                    NPNKey fullKey = new NPNKey(basesDecimal, keyDecimal);
        
                    Cipher cipher = new Feistel3(fullKey, 64);

                    byte[] prev = new byte[8];

                    byte[] init = {5, 12, 4, 2, 18, 33, 51, 71};

                    boolean first = false;
                    long startTime = System.currentTimeMillis();
                    while (bis.read(block) != -1) {
                        //System.out.println("======= BLOCK =========");
                        //System.out.println(new String(block, "cp1251"));

                        if (!first) {
                            first = true;
                            byte[] b = new byte[block.length];
                            for (int i = 0; i < block.length; i++) {
                                //System.out.println("First" + block.length);
                                b[i] = (byte) (block[i] ^ init[i]);
                            }
                            prev = cipher.encrypt(b);
                            bos.write(prev);
                        } else {
                            //System.out.println(block.length);
                            byte[] b = new byte[block.length];
                            for (int i = 0; i < block.length; i++) {
                                b[i] = (byte) (block[i] ^ prev[i]);
                            }
                            prev = cipher.encrypt(b);
                            bos.write(prev);
                        }

                        block = new byte[8];
                    }
                    long endTime = System.currentTimeMillis();
                    System.out.println("Nested Feistel NPNs Total execution time: " + (endTime-startTime) + "ms"); 
                    bos.flush();
                    

                    /*first = false;

                    while (dbis.read(block) != -1) {

                        if (!first) {
                            first = true;
                            prev = block;
                            byte[] d = cipher.decrypt(block);
                            byte[] b = new byte[block.length];
                            for (int i = 0; i < block.length; i++) {
                                b[i] = (byte) (d[i] ^ init[i]);
                            }
                            dbos.write(b);
                        } else {
                            byte[] d = cipher.decrypt(block);
                            byte[] b = new byte[block.length];
                            for (int i = 0; i < block.length; i++) {
                                b[i] = (byte) (d[i] ^ prev[i]);
                            }

                            dbos.write(b);
                            prev = block;
                        }
                        block = new byte[8];

                    }*/

                    if (bis != null) {
                        bis.close();
                    }
                    if (dbis != null) {
                        dbis.close();
                    }
                    if (bos != null) {
                        bos.close();
                    }
                    if (dbos != null) {
                        dbos.close();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
    }
}
