/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crypto;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import polynomial.BinaryHelpers;
import polynomial.BinaryPolynomial;
import polynomial.BinaryPolynomialMath;

/**
 *
 * @author User
 */
public class NPNKeyGenerator {
    
    public static NPNKey getNPNKey(int[] basesDecimal) {


	int[] keysDecimal = new int[basesDecimal.length];

        for(int i = 0; i < basesDecimal.length; i++) {
	    
	    BinaryPolynomial base = new BinaryPolynomial(basesDecimal[i]);
            keysDecimal[i] = randomIntForDegree(base.getDegree());
            System.out.println(base.getDegree() + " " + keysDecimal[i]);
	}
            
	return new NPNKey(basesDecimal, keysDecimal);
        
        
    }
    
    /**
     * Generates a secret key to be used in the encryption process
     * @return The secret key
     */
    public static SecretKey generateAES128Key() {
        KeyGenerator keygen;
        try {

            //Java normally doesn't support 256-bit key sizes without an extra installation so stick with a 128-bit key
            keygen = KeyGenerator.getInstance("AES");
            keygen.init(128);
            SecretKey aesKey = keygen.generateKey();
            return aesKey;
        }
        catch(NoSuchAlgorithmException ex) {
            System.out.println("Key generation error: " +ex);
            return null;
        }
    }
    
    private static byte[] randomBytes(int size) {
        SecureRandom random = new SecureRandom();
        
        byte[] bytes = new byte[size];
        
        random.nextBytes(bytes);
        
        return bytes;
    }
    
    private static int randomIntForDegree(int degree) {
        SecureRandom random = new SecureRandom();
        
        int result = 0;
               
        while(result <= 0) {
            result = random.nextInt() & ((int)Math.pow(2, degree) - 1);
        }

        
        return result;
    }
    
}
