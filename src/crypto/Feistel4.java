/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crypto;

import static crypto.SimpleCipher.MODE_ENCRYPT;
import java.util.Arrays;
import java.util.BitSet;
import java.util.stream.IntStream;
import polynomial.BinaryHelpers;
import polynomial.BinaryPolynomial;
import polynomial.BinaryPolynomialMath;

/**
 *
 * @author User
 */
public class Feistel4 implements Cipher {
    
    static int MODE_ENCRYPT = 0;
    static int MODE_DECRYPT = 1;
    
    protected int roundCount = 8;
    
    protected int blockSize = 128;
   
    protected NPNKey fullKey;
    
    protected SimpleCipher npnCipher;

    protected byte[] key;
    protected byte[] bases;
    
    public Feistel4(NPNKey fullKey) {
	this.fullKey = fullKey;  
        
        this.npnCipher = new SimpleCipher(fullKey, this.blockSize);
        
        this.key = BinaryHelpers.intArrayToByteArray(fullKey.getKeyDecimal());
        this.bases =  BinaryHelpers.intArrayToByteArray(fullKey.getBasesDecimal());
    }
    
    public Feistel4(NPNKey fullKey, int blockSize) {
	this.fullKey = fullKey;  
        
        this.blockSize = blockSize;
        
        this.npnCipher = new SimpleCipher(fullKey, this.blockSize / 2);
        
        this.key = BinaryHelpers.intArrayToByteArray(fullKey.getKeyDecimal());
        
    }
    
    @Override
    public byte[] encrypt(byte[] input) {
	return this.process(input, MODE_ENCRYPT);
    }

    @Override
    public byte[] decrypt(byte[] input) {
        return this.process(input, MODE_DECRYPT);

    }

     public int getBlockSize() {
        return blockSize;
    }

    public void setBlockSize(int blockSize) {
        this.blockSize = blockSize;
    }
    
    protected byte[] process(byte[] input, int mode) {
        //System.out.println("MAIN KEY  " + BinaryHelpers.toBinaryString(key, 64));
        byte[] left = new byte[input.length / 2];
        byte[] right = new byte[input.length / 2];
        
        byte[][] roundKeys = new byte[roundCount][];

        for(int i = 0; i < roundCount; i++ ) {
            roundKeys[i] = getRoundKey(i);
        }

        System.arraycopy(input, 0, left, 0, left.length);
        System.arraycopy(input, left.length, right, 0, right.length);
        
        byte[] roundKey;
                
        for(int i = 0; i < roundCount; i++ ) {
            
            if(mode == MODE_ENCRYPT) {
                roundKey = roundKeys[i];
            } else {
                roundKey = roundKeys[(roundCount - 1) - i];
            }
    
            final byte[] temp = f(left, roundKey);
                       
           // System.out.println("In Round  " + i + ": " + BinaryHelpers.toBinaryString(left, 32) + "|" + BinaryHelpers.toBinaryString(right, 32));
            BitSet beforeBits = BitSet.valueOf(temp);
            BitSet afterBits = new BitSet(32);
            //System.out.println("Before " + BinaryHelpers.toBinaryString(temp, 32));
            //Permutation
            for (int j = beforeBits.nextSetBit(0); j >= 0; j = beforeBits.nextSetBit(j + 1)) {
                int byteNum = j / 4;
                
                int to = (j * 8) % 32 + byteNum;
                afterBits.set(to);
               // System.out.println("After " + byteNum + " " + j + " - " + to);
            }
            byte[] temp123 = afterBits.toByteArray();
            for(int j = 0; j < temp.length; j++) {
                if(temp123.length <= j) {
                    temp[j] = 0;
                } else {
                    temp[j] = temp123[j];
                }
            }
            //S-block
            //System.out.println("After " + BinaryHelpers.toBinaryString(temp, 32));
            final byte[] rightTemp = right;
            IntStream.range(0, temp.length).parallel().forEach(j -> {
                int val = temp[j] < 0 ? temp[j] & 0xFF: temp[j];
                temp[j] = (byte)(this.sblock(val) ^ rightTemp[j]);
            });

           // System.out.println("Round  " + i + ": " + BinaryHelpers.toBinaryString(temp, 32));
            right = left;
            left = temp;
            
        }
        
        return BinaryHelpers.concat(right, left);
    }
    
    protected byte[] f(byte[] subblock, byte[] key) {
        
        byte[] result = this.npnCipher.encrypt(subblock);
        
        for(int j = 0; j < result.length; j++) {
            result[j] = (byte)(result[j] ^ key[j]);
        }
        
        return result;
    }
    
    protected byte[] getRoundKey(int roundNum) {
        BinaryPolynomial p = this.fullKey.bases.get(roundNum % this.fullKey.bases.size());
        byte[] keys = Arrays.copyOfRange(BinaryHelpers.shiftBitsLeft(this.key, (roundNum * p.getDegree())), 0, this.blockSize / 2);
        for(int j = 0; j < keys.length; j++) {
                int val = keys[j] < 0 ? keys[j] & 0xFF: keys[j];
                keys[j] = (byte)this.sblock((val + j * roundNum * p.getDegree()) % 255);
            }
        //System.out.println("KEY of " + roundNum + " " + BinaryHelpers.toBinaryString(keys, 32));
        return keys;
    }
    
    protected static byte[] shuffle(byte[] a) {
        int aLen = a.length;
        int middle = aLen / 2;
        byte[] c = new byte[aLen];
        System.arraycopy(a, 0, c, middle, middle);
        System.arraycopy(a, middle, c, 0, middle);
        return c;
    }
    
    private short[] s = {37,	127,	46,	108,	23,	56,	96,	51,	141,	33,	120,	84,	75,	5,	103,	69,

151,	9,	111,	185,	137,	146,	15,	171,	115,	191,	41,	181,	123,	132,	62,	6,

85,	101,	174,	27,	73,	226,	65,	211,	20,	149,	223,	36,	95,	113,	188,	39,

32,	164,	77,	202,	121,	144,	161,	91,	155,	60,	126,	153,	175,	24,	167,	0,

117,	180,	218,	43,	183,	12,	233,	204,	30,	213,	178,	52,	136,	162,	99,	159,

70,	195,	22,	237,	81,	220,	48,	107,	229,	87,	2,	109,	255,	68,	200,	17,

173,	40,	187,	55,	114,	245,	130,	241,	94,	209,	234,	193,	57,	197,	47,	147,

90,	232,	214,	243,	217,	61,	236,	1,	166,	35,	134,	216,	3,	118,	143,	72,

157,	7,	59,	206,	247,	254,	64,	252,	170,	199,	239,	14,	222,	190,	88,	179,

76,	168,	249,	160,	13,	251,	230,	242,	253,	71,	250,	246,	102,	19,	224,	129,

208,	26,	145,	98,	248,	44,	227,	125,	11,	156,	92,	231,	58,	212,	139,	78,

140,	54,	198,	221,	244,	201,	215,	240,	219,	238,	53,	203,	186,	110,	31,	172,

18,	225,	165,	86,	34,	235,	104,	29,	176,	112,	228,	8,	131,	207,	194,	25,

133,	189,	49,	124,	192,	119,	210,	205,	63,	82,	182,	38,	150,	42,	163,	122,

105,	4,	177,	184,	66,	196,	16,	154,	93,	106,	74,	142,	135,	158,	97,	10,

67,	148,	89,	28,	152,	80,	138,	100,	50,	169,	21,	128,	83,	45,	116,    79
};
    
    protected int sblock(int in) {
        //System.out.println("S " + in);
        return s[in];
    }
}
