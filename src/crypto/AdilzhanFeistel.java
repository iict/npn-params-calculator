/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crypto;

import static crypto.SimpleCipher.MODE_ENCRYPT;
import java.time.LocalDateTime;
import java.time.temporal.ChronoField;
import java.util.Arrays;
import polynomial.BinaryHelpers;
import polynomial.BinaryPolynomial;
import polynomial.BinaryPolynomialMath;

/**
 *
 * @author User
 */
public class AdilzhanFeistel implements Cipher {
    
    static int MODE_ENCRYPT = 0;
    static int MODE_DECRYPT = 1;
    
    protected int roundCount = 4;
    
    protected int blockSize = 128;
   
    protected byte[] iv;

    protected NPNKey fullKey;
    
    protected SimpleCipher npnCipher;

    protected byte[] key;
    protected byte[] bases;
    
    
    public AdilzhanFeistel(NPNKey fullKey) {
	this.fullKey = fullKey;  
        
        this.npnCipher = new SimpleCipher(fullKey, this.blockSize);
        
        this.key = BinaryHelpers.intArrayToByteArray(fullKey.getKeyDecimal());
        this.bases =  BinaryHelpers.intArrayToByteArray(fullKey.getBasesDecimal());
    }
    
    public AdilzhanFeistel(NPNKey fullKey, int blockSize) {
	this.fullKey = fullKey;  
        
        this.blockSize = blockSize;
        
        this.npnCipher = new SimpleCipher(fullKey, this.blockSize);
        
        this.iv = new byte[this.blockSize / 2];
        
        this.key = BinaryHelpers.intArrayToByteArray(fullKey.getKeyDecimal());
        
    }
    
    public AdilzhanFeistel(NPNKey fullKey, int blockSize, int roundCount, byte[] iv) {
	this.fullKey = fullKey;  
        
        this.blockSize = blockSize;
        
        this.roundCount = roundCount;
        
        this.iv = iv;
            
        this.npnCipher = new SimpleCipher(fullKey, this.blockSize);
        
        this.key = BinaryHelpers.intArrayToByteArray(fullKey.getKeyDecimal());
        
    }
    
    public byte[] getIv() {
        return iv;
    }

    public void setIv(byte[] iv) {
        this.iv = iv;
    }
    
    
    @Override
    public byte[] encrypt(byte[] input) {
        //System.out.println(this.npnCipher.encrypt(this.process(input, MODE_ENCRYPT)).length);
	return this.process(input, MODE_ENCRYPT);
    }

    @Override
    public byte[] decrypt(byte[] input) {
        return this.process(input, MODE_DECRYPT);

    }

    public int getBlockSize() {
        return blockSize;
    }

    public void setBlockSize(int blockSize) {
        this.blockSize = blockSize;
    }
    
    protected byte[] process(byte[] input, int mode) {
        
        long startTime = LocalDateTime.now().getLong(ChronoField.MICRO_OF_SECOND);
        
        byte[] left = new byte[input.length / 2];
        byte[] right = new byte[input.length / 2];

        System.arraycopy(input, 0, left, 0, left.length);
        System.arraycopy(input, left.length, right, 0, right.length);
             
        /*try {
        System.out.println("IV: " + new String(iv, "UTF-8"));
        } catch(Exception e) {   
        }*/
        for(int i = 0; i < roundCount; i++ ) {
            //System.out.println("Round #" + i);
            
            if(mode == MODE_ENCRYPT) {
                //System.out.println("Round #" + i);
                if(i == 0) {
                    for(int j = 0; j < left.length; j++) {
                        left[j] = (byte)(left[j] ^ iv[j]);
                    }
                }
                
                byte[] temp  = this.npnCipher.encrypt(left);
                
                for(int j = 0; j < temp.length; j++) {
                    temp[j] = (byte)(temp[j] ^ right[j]);
                }
                
             
                right = left;
                left = temp;
                
                //System.out.println(BinaryHelpers.toBinaryString(BinaryHelpers.concat(left,right), this.blockSize));
               
            } 
            
            if(mode == MODE_DECRYPT) {
                byte[] temp = this.npnCipher.encrypt(left);
                
                for(int j = 0; j < temp.length; j++) {
                   temp[j] = (byte)(temp[j] ^ right[j]);
                }
                
                if(i == roundCount - 1) {
                    for(int j = 0; j < left.length; j++) {
                        left[j] = (byte)(left[j] ^ iv[j]);
                    }
                }
                right = left;
                left = temp;
            }
  
        }
        if(mode == MODE_ENCRYPT) {
            long endTime = LocalDateTime.now().getLong(ChronoField.MICRO_OF_SECOND);
            System.out.println(this.blockSize + " bit " + this.roundCount + " round execution time: " + (endTime-startTime) + " us"); 
        }
                    
        return BinaryHelpers.concat(right, left);
    }
    
    
}
