/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crypto;

import java.util.List;
import java.util.stream.IntStream;
import polynomial.BinaryHelpers;
import polynomial.BinaryPolynomial;
import polynomial.BinaryPolynomialMath;

public class SimpleCompress implements Cipher {

    static int MODE_ENCRYPT = 0;
    static int MODE_DECRYPT = 1;
    
    protected NPNKey fullKey;

    protected int blockSize = 128;
    
    public SimpleCompress(NPNKey fullKey) {
	this.fullKey = fullKey;    
    }

    public SimpleCompress(NPNKey fullKey, int blockSize) {
	this.fullKey = fullKey;    
        this.blockSize = blockSize;
    }
     
    @Override
    public byte[] encrypt(byte[] input) {
	return this.process(input, MODE_ENCRYPT);
    }

    @Override
    public byte[] decrypt(byte[] input) {
	return this.process(input, MODE_DECRYPT);
    }
    
    public int getBlockSize() {
        return blockSize;
    }

    public void setBlockSize(int blockSize) {
        this.blockSize = blockSize;
    }
    
    protected byte[] process(byte[] input, int mode) {
	int keyIndex = 0;
	int prevIndex = 0;

	String resultArr = "";

        String bitStr = BinaryHelpers.toBinaryString(input, this.blockSize);


	for (BinaryPolynomial base : fullKey.getBases()) {
            //System.out.println("USing base " + base.toString());
	    BinaryPolynomial dt = new BinaryPolynomial(bitStr.substring(prevIndex, prevIndex + base.getDegree()));
                
	    prevIndex = prevIndex + base.getDegree();


            BinaryPolynomial result;
	   
	    if(mode == MODE_ENCRYPT) {

		result = BinaryPolynomialMath.mod(BinaryPolynomialMath.multiple(dt, fullKey.getKey().get(keyIndex)), base);

	    }
	    else {

		result = BinaryPolynomialMath.mod(BinaryPolynomialMath.multiple(dt, fullKey.getInverseKey().get(keyIndex)), base);

	    }
           
	    resultArr +=result.toFullString(base.getDegree());
        
	    keyIndex++;

    }
	return BinaryHelpers.fromBinaryString(resultArr);
    }

    protected byte[] concat(byte[] a, byte[] b) {
	int aLen = a.length;
	int bLen = b.length;
	byte[] c = new byte[aLen + bLen];
	System.arraycopy(a, 0, c, 0, aLen);
	System.arraycopy(b, 0, c, aLen, bLen);
	return c;
    }

     /*protected byte[] process(byte[] input, int mode) {
	int keyIndex = 0;
	final int prevIndex = 0;

	String resultArr = "";

        String bitStr = BinaryHelpers.toBinaryString(input, this.blockSize);

        final String[] res = new String[fullKey.getBases().size()];

        IntStream.range(0, fullKey.getBases().size()).parallel().forEach(i -> {
            BinaryPolynomial dt, base;
            base = fullKey.getBases().get(i);
            if(i == 0) {
                dt = new BinaryPolynomial(bitStr.substring(0, base.getDegree()));
            } else {
                int j = i - 1;
                int prev = fullKey.getBases().get(j).getDegree();
                while(j > 0) {
                    j--; 
                    prev += fullKey.getBases().get(j).getDegree();
                }
                
                dt = new BinaryPolynomial(bitStr.substring(prev, prev + base.getDegree()));
            }
            BinaryPolynomial result;
	   
	    if(mode == MODE_ENCRYPT) {
		result = BinaryPolynomialMath.mod(BinaryPolynomialMath.multiple(dt, fullKey.getKey().get(i)), base);
	    }
	    else {
		result = BinaryPolynomialMath.mod(BinaryPolynomialMath.multiple(dt, fullKey.getInverseKey().get(i)), base);
	    }
            res[i] = result.toFullString(base.getDegree());
           
        });
        
	return BinaryHelpers.fromBinaryString(String.join("", res));
    }*/
  
}
