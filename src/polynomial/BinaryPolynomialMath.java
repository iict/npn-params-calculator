/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polynomial;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.IntStream;

/**
 *
 * @author Мирас
 */
public class BinaryPolynomialMath {
    
    public static BinaryPolynomial multiple(BinaryPolynomial x, BinaryPolynomial y) {
    
	BitSet a,b;
	
	if(y.getBits().length() > x.getBits().length()) {
	    a = (BitSet)y.getBits().clone();
	    b = (BitSet)x.getBits().clone();
	} else {
	    a = (BitSet)x.getBits().clone();
	    b = (BitSet)y.getBits().clone();
	} 
	
	BitSet result = new BitSet();
	
	for(int i = 0; i < b.length(); i++) {
	    
	    BitSet temp = new BitSet();
	    int k = i;
	    for(int j = 0; j < a.length(); j++) {
                if(b.get(i) && a.get(j)) {
		    temp.set(k);
		}
                k++;
		}
	    result.xor(temp);
	}

	return new BinaryPolynomial(result);
    }
    
    public static DivisionResult divide(BinaryPolynomial ia, BinaryPolynomial ib) {
	
	BinaryPolynomial quotient, remainder;
	BinaryPolynomial a = new BinaryPolynomial(ia.getBits());
	BinaryPolynomial b = new BinaryPolynomial(ib.getBits());
	
	if(a.getBits().length() < b.getBits().length()) {
	    quotient = new BinaryPolynomial();
	    remainder = a;
	    return new DivisionResult(quotient, remainder);
	}
	
	BitSet n = a.getBits();
	BitSet q = new BitSet();
	
	int K = n.length() - 1;
	int N = b.getDegree();
	int i = 0;
	
	while(n.length() - 1 >= N) {
	    
	    if(n.get(K - i)) {
	    
		BinaryPolynomial x = BinaryPolynomial.getForDegree(K - N - i);

		n.xor(BinaryPolynomialMath.multiple(x, b).getBits());

		BitSet bs = new BitSet();
		bs.set(K - N - i);

		q.xor(bs); 
	    }
    
	    i++;	    
	}
	
	quotient = new BinaryPolynomial(q);
	remainder = new BinaryPolynomial(n);
	
	return new DivisionResult(quotient, remainder);
    }
    
       
    public static BinaryPolynomial gcd(BinaryPolynomial ia, BinaryPolynomial ib) {
	
	if(ib.getBits().isEmpty()) {
	    BinaryPolynomial a = new BinaryPolynomial(ia.getBits());
	    return a;
	}
	
	return BinaryPolynomialMath.gcd(ib, BinaryPolynomialMath.mod(ia, ib));
	
    }
    
     public static BinaryPolynomial[] ExtendedEuclid(BinaryPolynomial a, BinaryPolynomial b)
    /*  This function will perform the Extended Euclidean algorithm
        to find the GCD of a and b.  We assume here that a and b
        are non-negative (and not both zero).  This function also
        will return numbers j and k such that 
               d = j*a + k*b
        where d is the GCD of a and b.
	     
    */
    { 
        System.out.println(a+ " " + b);
        BinaryPolynomial[] ans = new BinaryPolynomial[3];
        DivisionResult q;
	
        if (b.getBits().isEmpty())  {  /*  If b = 0, then we're done...  */
            ans[0] = new BinaryPolynomial(a.getBits());
            ans[1] = new BinaryPolynomial("1");
            ans[2] = new BinaryPolynomial("0");
        }
        else
            {     /*  Otherwise, make a recursive function call  */ 
               q = BinaryPolynomialMath.divide(a, b);
               ans = ExtendedEuclid (b, BinaryPolynomialMath.mod(a, b));
               BinaryPolynomial temp = BinaryPolynomialMath.xor(ans[1], BinaryPolynomialMath.multiple(ans[2], q.quotient));
               ans[1] = ans[2];
               ans[2] = temp;
            }
        System.out.println(ans[0] + " " + ans[1] + " " +  ans[2]);
        return ans;
    }
   
    
    public static BinaryPolynomial mod(BinaryPolynomial a, BinaryPolynomial x) {
	DivisionResult dr = BinaryPolynomialMath.divide(a, x);
	return dr.getRemainder();
    }
    
    public static BinaryPolynomial xor(BinaryPolynomial ia, BinaryPolynomial x) {
	
	BinaryPolynomial a = new BinaryPolynomial(ia.getBits());

	a.getBits().xor(x.getBits());
	
	return a;
    }
    
    public static Set primeFactors(final int number) {
        int i;
        Set primefactors = new HashSet<>();
        int copyOfInput = number;

        for (i = 2; i <= copyOfInput; i++) {
            if (copyOfInput % i == 0) {
                primefactors.add(i); // prime factor
                copyOfInput /= i;
                i--;
            }
        }
        return primefactors;
    }
    
}
