/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polynomial;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.BitSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Мирас
 */
public class BinaryPolynomial {
    
    protected BitSet bitSet;
    
    public BinaryPolynomial() {
	this.bitSet = new BitSet();
    }
    
    public BinaryPolynomial(BitSet bs) {
	this.bitSet = (BitSet)bs.clone();
    }
    
    public BinaryPolynomial(byte[] bytes) {
	this.bitSet = BitSet.valueOf(bytes);
    }
    
    public BinaryPolynomial(String bytesStr) {
	
	this.bitSet = new BitSet();
	
	for(int i = 0; i < bytesStr.length() ; i++) {
	    if(bytesStr.charAt(i) == '1')
		this.bitSet.set(bytesStr.length() - 1 - i);
	}
    }
    
    public BinaryPolynomial(Integer dec) {
	ByteBuffer buff = ByteBuffer.allocate(4);
	buff.order(ByteOrder.LITTLE_ENDIAN);
	buff.putInt(dec);
	this.bitSet = BitSet.valueOf(buff.array());
    }
    
    public BitSet getBits() {
	return this.bitSet;
    }
    
    public void setBits(BitSet b) {
	this.bitSet = b;
    }
    
    public int getDegree() {
	return this.bitSet.length() - 1;
    }
    
    public boolean isIrreducible() {
	
	return this.getReducibilityRabin(BinaryPolynomialMath.primeFactors(this.getDegree()));
    }

    
    /*
	returns polynomial 1*x^d + 0;
    */
    public static BinaryPolynomial getForDegree(int d) {
	BitSet bs = new BitSet();
	bs.set(d);
	
	return new BinaryPolynomial(bs);
    }
    
    public int toInt() {
        
        byte[] bytes = {0x00, 0x00, 0x00, 0x00};
        
        byte[] bits = this.bitSet.toByteArray();
        
        System.arraycopy(bits, 0, bytes, 0, bits.length);
        
        return BinaryHelpers.byteArrayToInt(bytes);
    }
    
    @Override
    public String toString() {
	
	String result = this.bitSet.length() > 0 ? "" : "0";
	
	for(int i = this.bitSet.length() - 1; i >= 0; i--) {
	    if(this.bitSet.get(i)) {
		result+= "1";
	    } else {
		result+= "0";
	    }
	}
	
	return result;
    }

    public String toFullString(int bitCount) {
        
        String result = "";
	
	for(int i = bitCount - 1; i >= 0; i--) {
	    if(this.bitSet.get(i)) {
		result+= "1";
	    } else {
		result+= "0";
	    }
	}
	
	return result;
    }
    /**
    * Rabin's Reducibility Test
    * 
    * 
    */
    protected boolean getReducibilityRabin(Set<Integer> factors) {
	int degree = (int) this.getDegree();
	
	//create polynomial 1;
	BitSet one = new BitSet();
	one.set(0);
	//System.out.println(factors);
	for (Integer factor : factors) {
		int n_i = degree / factor;	
		BinaryPolynomial b = reduceExponent(n_i);
		BinaryPolynomial g = BinaryPolynomialMath.gcd(b, this);
		if (!g.getBits().equals(one))
			return false;
	}

	BinaryPolynomial g = reduceExponent(degree);
	
	return g.getBits().isEmpty();

    }
    
    /**
    * Computes ( x^(2^p) - x ) mod f
    * 
    * This function is useful for computing the reducibility of the polynomial
    */
    private BinaryPolynomial reduceExponent(final int p) {
	    // compute (x^q^p mod f)
	    int q_to_p = (int) Math.pow(2.0, p);
	    
	    //x^(2^p)
	    BitSet bsx = new BitSet();
	    bsx.set(q_to_p);

	    //x
	    BitSet bx = new BitSet();
	    bx.set(1);
	
	    //( x^(2^p) - x )
	    bsx.xor(bx);
	    
	    BinaryPolynomial exp = new BinaryPolynomial(bsx);
	    
	    return BinaryPolynomialMath.mod(exp, this);
    }
}
