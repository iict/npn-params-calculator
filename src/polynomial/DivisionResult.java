/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polynomial;

/**
 *
 * @author Мирас
 */
public class DivisionResult {
    
    protected BinaryPolynomial quotient;
    protected BinaryPolynomial remainder;
    
    public DivisionResult(BinaryPolynomial q, BinaryPolynomial r) {
	this.quotient = q;
	this.remainder = r;
    }

    public BinaryPolynomial getQuotient() {
	return quotient;
    }

    public void setQuotient(BinaryPolynomial quotient) {
	this.quotient = quotient;
    }

    public BinaryPolynomial getRemainder() {
	return remainder;
    }

    public void setRemainder(BinaryPolynomial remainder) {
	this.remainder = remainder;
    }
    
    @Override
    public String toString() {
	return "q=" +this.quotient + ", r=" + this.remainder;
    }
}
