/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polynomial;

import ui.DataService;
import crypto.AES;
import crypto.NPNKey;
import crypto.SimpleCipher;
import crypto.Cipher;
import crypto.DES;
import crypto.Feistel2;
import crypto.Feistel3;
import crypto.Feistel4;
import crypto.NPNKeyGenerator;
import crypto.SimpleFeistel;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFrame;
import javax.xml.bind.DatatypeConverter;
import ui.MainFrame;

/**
 *
 * @author Мирас
 */
public class PolynomialHelper {

    /**
     * @param args the command line arguments
     */
    private static Connection connect = null;
    private static Statement statement = null;
    private static PreparedStatement preparedStatement = null;
    
    public static String toHexString(byte[] array) {
        return DatatypeConverter.printHexBinary(array);
    }

    public static String toAsciiString(byte[] array) {
        return DatatypeConverter.printBase64Binary(array);
    }

    public static byte[] toByteArray(String s) {
        return DatatypeConverter.parseHexBinary(s);
    }

    public static void main(String[] args) {
        JFrame frame = new MainFrame(new DataService());
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
    
    
    public static void mainOld(String[] args) {

        String filename = "huge.psd";
       /* AES.testAES(filename);
        DES.testDES(filename);*/
        
       /* testNPNs();
        testNPNs64();
        Feistel3.testNPNs(filename);*/
        //Prepare keys
        //int[] basesDecimal = {67, 73, 87, 37, 41, 47, 55, 59, 19, 25, 31, 11, 13, 7, 3};
        //int[] keyDecimal = {31, 12, 60, 21, 21, 30, 12, 25, 3, 11, 3, 5, 2, 1, 1};
       /* System.out.println("APP started");

        List<BinaryPolynomial> bases = new ArrayList();
        bases.add(new BinaryPolynomial("1010001111100110001011111"));//23
        bases.add(new BinaryPolynomial("101110000011101010010001")); //65
        bases.add(new BinaryPolynomial("101111100011010101")); //59

        List<BinaryPolynomial> keys = new ArrayList();
        keys.add(new BinaryPolynomial("101010111011010001001010"));
        keys.add(new BinaryPolynomial("11110111011100100111110"));
        keys.add(new BinaryPolynomial("10000100101010010"));

        NPNKey fullkey = new NPNKey(bases, keys);
        
        Cipher cipher = new SimpleFeistel(fullkey, 64);
        
        byte[] encrypted = new byte[8];
        byte[] decrypted = new byte[8];
        String data = "0011011100101001111101001111100100010111101010111011010001001010";
        byte[] input = BinaryHelpers.fromBinaryString(data);
        encrypted = cipher.encrypt(input);
        decrypted = cipher.decrypt(encrypted);
        
        String out = BinaryHelpers.toBinaryString(decrypted, 64);
        
        System.out.println(out.equals(data));
        System.out.println(encrypted.length);
        System.out.println(BinaryHelpers.toBinaryString(encrypted, 64));*/
        
              

        BufferedInputStream bis = null, dbis = null;
        BufferedOutputStream bos = null, dbos = null;
        
        Integer fileIndex = 1;
       // for (String filename : PolynomialHelper.getFilenames()) {
            System.out.println("Using file " + fileIndex);
            Integer keyIndex = 1;
            for (NPNKey fullKey : PolynomialHelper.getKeys()) {
                System.out.println("Using key " + keyIndex + ". Bases " + fullKey.getBases().size() + " Max: " + fullKey.getBases().get(0).getDegree());
                try {
                    bis = new BufferedInputStream(new FileInputStream(
                            "bigtest/" + filename));
                    bos = new BufferedOutputStream(new FileOutputStream(
                            "bigtest/crypted/miras_" + fileIndex + "_" + keyIndex));

                    dbis = new BufferedInputStream(new FileInputStream(
                            "bigtest/crypted/miras_" + fileIndex + "_" + keyIndex));
                    dbos = new BufferedOutputStream(new FileOutputStream(
                            "bigtest/decrypted/" + fileIndex + "_" + keyIndex + "_" + filename));

                    byte[] block = new byte[8];

                    Cipher cipher = new SimpleFeistel(fullKey, 64);

                    byte[] prev = new byte[8];

                    byte[] init = {5, 12, 4, 2, 18, 33, 51, 71};

                    boolean first = false;
                    
                    long startTime = System.currentTimeMillis();
                    while (bis.read(block) != -1) {
                        //System.out.println("======= BLOCK =========");
                        //System.out.println(new String(block, "cp1251"));

                        if (!first) {
                            first = true;
                            byte[] b = new byte[block.length];
                            for (int i = 0; i < block.length; i++) {
                                //System.out.println("First" + block.length);
                                b[i] = (byte) (block[i] ^ init[i]);
                            }
                            prev = cipher.encrypt(b);
                            bos.write(prev);
                        } else {
                            //System.out.println(block.length);
                            byte[] b = new byte[block.length];
                            for (int i = 0; i < block.length; i++) {
                                b[i] = (byte) (block[i] ^ prev[i]);
                            }
                            prev = cipher.encrypt(b);
                            bos.write(prev);
                        }

                        block = new byte[8];
                    }
                    long endTime = System.currentTimeMillis();
                    System.out.println("Key " + keyIndex + " NPNs 64 Total execution time: " + (endTime-startTime) + " ms"); 
                    bos.flush();

                    first = false;
/*
                    while (dbis.read(block) != -1) {

                        if (!first) {
                            first = true;
                            prev = block;
                            byte[] d = cipher.decrypt(block);
                            byte[] b = new byte[block.length];
                            for (int i = 0; i < block.length; i++) {
                                b[i] = (byte) (d[i] ^ init[i]);
                            }
                            dbos.write(b);
                        } else {
                            byte[] d = cipher.decrypt(block);
                            byte[] b = new byte[block.length];
                            for (int i = 0; i < block.length; i++) {
                                b[i] = (byte) (d[i] ^ prev[i]);
                            }

                            dbos.write(b);
                            prev = block;
                        }
                        block = new byte[8];

                    }
*/
                    if (bis != null) {
                        bis.close();
                    }
                    if (dbis != null) {
                        dbis.close();
                    }
                    if (bos != null) {
                        bos.close();
                    }
                    if (dbos != null) {
                        dbos.close();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                keyIndex++;
            }
           // fileIndex++;
       // }
        
    }

   
   
   
    public static void testNPNs() {
        
            BufferedInputStream bis = null, dbis = null;
            BufferedOutputStream bos = null, dbos = null;
               try {
                   
                    String filename = "4.pdf";
                    bis = new BufferedInputStream(new FileInputStream(
                            "bigtest/" + filename));
                    bos = new BufferedOutputStream(new FileOutputStream(
                            "bigtest/crypted/miras_1" + filename));

                    dbis = new BufferedInputStream(new FileInputStream(
                            "bigtest/crypted/miras_" + filename));
                    dbos = new BufferedOutputStream(new FileOutputStream(
                            "bigtest/decrypted/" + filename));

                    byte[] block = new byte[16];

                    int[] basesDecimal = {
                        91, //6
                        557, // 9
                        445, // 8
                        545, //9
                        117, //6
                        529, // 9
                        505, // 8
                        601, //9
                        67, 73, 87, 37, 41, 47, 55, 59, 19, 25, 31, 11, 13, 7, 3 };
                    int[] keyDecimal = {92, 111, 63, 70, 45, 49, 73 ,36, 31, 12, 60, 21, 21, 30, 12, 25, 3, 11, 3, 5,  2,  1, 1 };
                    NPNKey fullKey = new NPNKey(basesDecimal, keyDecimal);
        
                    Cipher cipher = new SimpleFeistel(fullKey, 128);

                    byte[] prev = new byte[16];

                    byte[] init = {5, 12, 4, 2, 18, 33, 51, 71, 7, 12, 11, 0, 52, 35, 58, 1};

                    boolean first = false;
                    long startTime = System.currentTimeMillis();
                    while (bis.read(block) != -1) {
                        //System.out.println("======= BLOCK =========");
                        //System.out.println(new String(block, "cp1251"));

                        if (!first) {
                            first = true;
                            byte[] b = new byte[block.length];
                            for (int i = 0; i < block.length; i++) {
                                //System.out.println("First" + block.length);
                                b[i] = (byte) (block[i] ^ init[i]);
                            }
                            prev = cipher.encrypt(b);
                            bos.write(prev);
                        } else {
                            //System.out.println(block.length);
                            byte[] b = new byte[block.length];
                            for (int i = 0; i < block.length; i++) {
                                b[i] = (byte) (block[i] ^ prev[i]);
                            }
                            prev = cipher.encrypt(b);
                            bos.write(prev);
                        }

                        block = new byte[16];
                    }
                    long endTime = System.currentTimeMillis();
                    System.out.println("NPNs 128 Total execution time: " + (endTime-startTime) + "ms"); 
                    bos.flush();
                    

                    /*first = false;

                    while (dbis.read(block) != -1) {

                        if (!first) {
                            first = true;
                            prev = block;
                            byte[] d = cipher.decrypt(block);
                            byte[] b = new byte[block.length];
                            for (int i = 0; i < block.length; i++) {
                                b[i] = (byte) (d[i] ^ init[i]);
                            }
                            dbos.write(b);
                        } else {
                            byte[] d = cipher.decrypt(block);
                            byte[] b = new byte[block.length];
                            for (int i = 0; i < block.length; i++) {
                                b[i] = (byte) (d[i] ^ prev[i]);
                            }

                            dbos.write(b);
                            prev = block;
                        }
                        block = new byte[8];

                    }*/

                    if (bis != null) {
                        bis.close();
                    }
                    if (dbis != null) {
                        dbis.close();
                    }
                    if (bos != null) {
                        bos.close();
                    }
                    if (dbos != null) {
                        dbos.close();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
    }
    
     public static void testNPNs64() {
        
            BufferedInputStream bis = null, dbis = null;
            BufferedOutputStream bos = null, dbos = null;
               try {
                   
                    String filename = "4.pdf";
                    bis = new BufferedInputStream(new FileInputStream(
                            "bigtest/" + filename));
                    bos = new BufferedOutputStream(new FileOutputStream(
                            "bigtest/crypted/miras_1" + filename));

                    dbis = new BufferedInputStream(new FileInputStream(
                            "bigtest/crypted/miras_" + filename));
                    dbos = new BufferedOutputStream(new FileOutputStream(
                            "bigtest/decrypted/" + filename));

                    byte[] block = new byte[8];

                    //KEY5
                    List<BinaryPolynomial> bases = new ArrayList();
                    List<BinaryPolynomial> keys = new ArrayList();
                               
                     bases = new ArrayList();
                    bases.add(new BinaryPolynomial("10101011"));
                    bases.add(new BinaryPolynomial("1011011"));
                    bases.add(new BinaryPolynomial("101001"));
                    bases.add(new BinaryPolynomial("110111"));
                    bases.add(new BinaryPolynomial("111101"));
                    bases.add(new BinaryPolynomial("100101"));
                    bases.add(new BinaryPolynomial("111011"));
                    bases.add(new BinaryPolynomial("101111"));
                    bases.add(new BinaryPolynomial("10011"));
                    bases.add(new BinaryPolynomial("11111"));
                    bases.add(new BinaryPolynomial("11001"));
                    bases.add(new BinaryPolynomial("1101"));
                    bases.add(new BinaryPolynomial("1011"));
                    bases.add(new BinaryPolynomial("111"));
                    bases.add(new BinaryPolynomial("11"));

                    keys = new ArrayList();
                    keys.add(new BinaryPolynomial("1010101"));
                    keys.add(new BinaryPolynomial("110110"));
                    keys.add(new BinaryPolynomial("10001"));
                    keys.add(new BinaryPolynomial("00101"));
                    keys.add(new BinaryPolynomial("00011"));
                    keys.add(new BinaryPolynomial("01110"));
                    keys.add(new BinaryPolynomial("00010"));
                    keys.add(new BinaryPolynomial("10011"));
                    keys.add(new BinaryPolynomial("1110"));
                    keys.add(new BinaryPolynomial("1001"));
                    keys.add(new BinaryPolynomial("1100"));
                    keys.add(new BinaryPolynomial("101"));
                    keys.add(new BinaryPolynomial("010"));
                    keys.add(new BinaryPolynomial("01"));
                    keys.add(new BinaryPolynomial("1"));
       
        
                    NPNKey fullKey = new NPNKey(bases, keys);
                    int[] basesDecimal = {67, 73, 87, 37, 41, 47, 55, 59, 19, 25, 31, 11, 13, 7, 3 };
                    int[] keyDecimal = {31, 12, 60, 21, 21, 30, 12, 25, 3, 11, 3, 5,  2,  1, 1 };

                    //NPNKey fullKey = new NPNKey(basesDecimal, keyDecimal);
                    //NPNKey fullKey = new NPNKey(basesDecimal, keyDecimal);
        
                    Cipher cipher = new SimpleFeistel(fullKey, 64);

                    byte[] prev = new byte[8];

                    byte[] init = {5, 12, 4, 2, 18, 33, 51, 71};

                    boolean first = false;
                    long startTime = System.currentTimeMillis();
                    while (bis.read(block) != -1) {
                        //System.out.println("======= BLOCK =========");
                        //System.out.println(block.length + "  " + prev.length);

                        if (!first) {
                            first = true;
                            byte[] b = new byte[block.length];
                            for (int i = 0; i < block.length; i++) {
                                //System.out.println("First" + block.length);
                                b[i] = (byte) (block[i] ^ init[i]);
                            }
                            prev = cipher.encrypt(b);
                            bos.write(prev);
                        } else {
                            //System.out.println(block.length);
                            byte[] b = new byte[block.length];
                            for (int i = 0; i < block.length; i++) {
                                b[i] = (byte) (block[i] ^ prev[i]);
                            }
                            prev = cipher.encrypt(b);
                            bos.write(prev);
                        }

                        block = new byte[8];
                    }
                    long endTime = System.currentTimeMillis();
                    System.out.println("NPNs 64 Total execution time: " + (endTime-startTime) + "ms"); 
                    bos.flush();
                    

                    /*first = false;

                    while (dbis.read(block) != -1) {

                        if (!first) {
                            first = true;
                            prev = block;
                            byte[] d = cipher.decrypt(block);
                            byte[] b = new byte[block.length];
                            for (int i = 0; i < block.length; i++) {
                                b[i] = (byte) (d[i] ^ init[i]);
                            }
                            dbos.write(b);
                        } else {
                            byte[] d = cipher.decrypt(block);
                            byte[] b = new byte[block.length];
                            for (int i = 0; i < block.length; i++) {
                                b[i] = (byte) (d[i] ^ prev[i]);
                            }

                            dbos.write(b);
                            prev = block;
                        }
                        block = new byte[8];

                    }*/

                    if (bis != null) {
                        bis.close();
                    }
                    if (dbis != null) {
                        dbis.close();
                    }
                    if (bos != null) {
                        bos.close();
                    }
                    if (dbos != null) {
                        dbos.close();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
    }
    public static List<String> getFilenames() {

        ArrayList<String> filenames = new ArrayList();
        filenames.add("1.docx");
        filenames.add("2.xls");
        filenames.add("3.pptx");
        filenames.add("4.pdf");
        filenames.add("5.rar");
        filenames.add("6.zip");
        filenames.add("7.vcf");
        filenames.add("8.jpg");
        filenames.add("9.png");
        filenames.add("10");
        filenames.add("11.html");
        filenames.add("12.gif");
        filenames.add("13.cat");
        filenames.add("14.mp4");
        filenames.add("15.wmz");
        filenames.add("16.gif");
        filenames.add("17.dll");
        filenames.add("18.log");
        filenames.add("19.lex");
        filenames.add("20.djvu");
        filenames.add("21.xml");
        filenames.add("22.mp3");

        return filenames;
    }

    public static List<NPNKey> getKeys() {

        List<NPNKey> fullkeys = new ArrayList();
        //KEY1
        List<BinaryPolynomial> bases = new ArrayList();
        bases.add(new BinaryPolynomial("1010001111100110001011111"));
        bases.add(new BinaryPolynomial("101110000011101010010001"));
        bases.add(new BinaryPolynomial("101111100011010101"));

        List<BinaryPolynomial> keys = new ArrayList();
        keys.add(new BinaryPolynomial("101010111011010001001010"));
        keys.add(new BinaryPolynomial("00110111000010100111110"));
        keys.add(new BinaryPolynomial("10000100101010010"));

        fullkeys.add(new NPNKey(bases, keys));

        //KEY2
        bases = new ArrayList();
        bases.add(new BinaryPolynomial("101111001101111100111"));
        bases.add(new BinaryPolynomial("110111010011010011"));
        bases.add(new BinaryPolynomial("110011011101001"));
        bases.add(new BinaryPolynomial("10001111"));
        bases.add(new BinaryPolynomial("1100001"));

        keys = new ArrayList();
        keys.add(new BinaryPolynomial("10101011101101000100"));
        keys.add(new BinaryPolynomial("10100011011100001"));
        keys.add(new BinaryPolynomial("01001111101001"));
        keys.add(new BinaryPolynomial("1100101"));
        keys.add(new BinaryPolynomial("010011"));

        fullkeys.add(new NPNKey(bases, keys));

        //KEY3
        bases = new ArrayList();
        bases.add(new BinaryPolynomial("110010111011110111"));
        bases.add(new BinaryPolynomial("1100111001100101"));
        bases.add(new BinaryPolynomial("1110110101"));
        bases.add(new BinaryPolynomial("10111001"));
        bases.add(new BinaryPolynomial("1100111"));
        bases.add(new BinaryPolynomial("101111"));
        bases.add(new BinaryPolynomial("1011"));
        bases.add(new BinaryPolynomial("111"));

        keys = new ArrayList();
        keys.add(new BinaryPolynomial("10101011101101000"));
        keys.add(new BinaryPolynomial("100101000110111"));
        keys.add(new BinaryPolynomial("000010100"));
        keys.add(new BinaryPolynomial("1111101"));
        keys.add(new BinaryPolynomial("001110"));
        keys.add(new BinaryPolynomial("01010"));
        keys.add(new BinaryPolynomial("100"));
        keys.add(new BinaryPolynomial("11"));

        fullkeys.add(new NPNKey(bases, keys));

        //KEY4
        bases = new ArrayList();
        bases.add(new BinaryPolynomial("1011110101"));
        bases.add(new BinaryPolynomial("101101001"));
        bases.add(new BinaryPolynomial("11010101"));
        bases.add(new BinaryPolynomial("101111"));
        bases.add(new BinaryPolynomial("111011"));
        bases.add(new BinaryPolynomial("111101"));
        bases.add(new BinaryPolynomial("110111"));
        bases.add(new BinaryPolynomial("11001"));
        bases.add(new BinaryPolynomial("11111"));
        bases.add(new BinaryPolynomial("10011"));
        bases.add(new BinaryPolynomial("1011"));
        bases.add(new BinaryPolynomial("1101"));
        bases.add(new BinaryPolynomial("111"));

        keys = new ArrayList();
        keys.add(new BinaryPolynomial("101010111"));
        keys.add(new BinaryPolynomial("01101000"));
        keys.add(new BinaryPolynomial("1001010"));
        keys.add(new BinaryPolynomial("00110"));
        keys.add(new BinaryPolynomial("11100"));
        keys.add(new BinaryPolynomial("00101"));
        keys.add(new BinaryPolynomial("00111"));
        keys.add(new BinaryPolynomial("1101"));
        keys.add(new BinaryPolynomial("0011"));
        keys.add(new BinaryPolynomial("1001"));
        keys.add(new BinaryPolynomial("010"));
        keys.add(new BinaryPolynomial("100"));
        keys.add(new BinaryPolynomial("11"));

        fullkeys.add(new NPNKey(bases, keys));

        //KEY5
        bases = new ArrayList();
        bases.add(new BinaryPolynomial("10101011"));
        bases.add(new BinaryPolynomial("1011011"));
        bases.add(new BinaryPolynomial("101001"));
        bases.add(new BinaryPolynomial("110111"));
        bases.add(new BinaryPolynomial("111101"));
        bases.add(new BinaryPolynomial("100101"));
        bases.add(new BinaryPolynomial("111011"));
        bases.add(new BinaryPolynomial("101111"));
        bases.add(new BinaryPolynomial("10011"));
        bases.add(new BinaryPolynomial("11111"));
        bases.add(new BinaryPolynomial("11001"));
        bases.add(new BinaryPolynomial("1101"));
        bases.add(new BinaryPolynomial("1011"));
        bases.add(new BinaryPolynomial("111"));
        bases.add(new BinaryPolynomial("11"));

        keys = new ArrayList();
        keys.add(new BinaryPolynomial("1010101"));
        keys.add(new BinaryPolynomial("110110"));
        keys.add(new BinaryPolynomial("10001"));
        keys.add(new BinaryPolynomial("00101"));
        keys.add(new BinaryPolynomial("00011"));
        keys.add(new BinaryPolynomial("01110"));
        keys.add(new BinaryPolynomial("00010"));
        keys.add(new BinaryPolynomial("10011"));
        keys.add(new BinaryPolynomial("1110"));
        keys.add(new BinaryPolynomial("1001"));
        keys.add(new BinaryPolynomial("1100"));
        keys.add(new BinaryPolynomial("101"));
        keys.add(new BinaryPolynomial("010"));
        keys.add(new BinaryPolynomial("01"));
        keys.add(new BinaryPolynomial("1"));

        fullkeys.add(new NPNKey(bases, keys));
        
        //KEY6
        bases = new ArrayList();
        bases.add(new BinaryPolynomial("1010011010100000011001101"));
        bases.add(new BinaryPolynomial("1110001101110100101"));
        bases.add(new BinaryPolynomial("‭10111110110001"));
        bases.add(new BinaryPolynomial("1001001011"));
        
        keys = new ArrayList();
        keys.add(new BinaryPolynomial("101000110000000111111001"));
        keys.add(new BinaryPolynomial("011100111001111010"));
        keys.add(new BinaryPolynomial("1010000101000‬"));
        keys.add(new BinaryPolynomial("010010101"));
        
        fullkeys.add(new NPNKey(bases, keys));
        
        //KEY7
        bases = new ArrayList();
        bases.add(new BinaryPolynomial("11101000010001001111001"));
        bases.add(new BinaryPolynomial("11001010001"));
        bases.add(new BinaryPolynomial("11000010101"));
        bases.add(new BinaryPolynomial("111100111"));
        bases.add(new BinaryPolynomial("100011011"));
        bases.add(new BinaryPolynomial("1100001"));
        
        keys = new ArrayList();
        keys.add(new BinaryPolynomial("1101100111001001100010"));
        keys.add(new BinaryPolynomial("1011010010"));
        keys.add(new BinaryPolynomial("1000010001"));
        keys.add(new BinaryPolynomial("00101010"));
        keys.add(new BinaryPolynomial("11100111"));
        keys.add(new BinaryPolynomial("010110"));
                
        fullkeys.add(new NPNKey(bases, keys));
        
        //KEY8
        bases = new ArrayList();
        bases.add(new BinaryPolynomial("101000110011001"));
        bases.add(new BinaryPolynomial("1011000001001"));
        bases.add(new BinaryPolynomial("100111001"));
        bases.add(new BinaryPolynomial("10000011"));
        bases.add(new BinaryPolynomial("111101"));
        bases.add(new BinaryPolynomial("101111"));
        bases.add(new BinaryPolynomial("100101"));
        bases.add(new BinaryPolynomial("11001"));
        bases.add(new BinaryPolynomial("10011"));
        
        keys = new ArrayList();
        keys.add(new BinaryPolynomial("01101001100011"));
        keys.add(new BinaryPolynomial("111100011010"));
        keys.add(new BinaryPolynomial("10011100"));
        keys.add(new BinaryPolynomial("1001010"));
        keys.add(new BinaryPolynomial("00100"));
        keys.add(new BinaryPolynomial("0‭1101"));
        keys.add(new BinaryPolynomial("0‭1010"));
        keys.add(new BinaryPolynomial("1111"));
        keys.add(new BinaryPolynomial("1110"));
                
        fullkeys.add(new NPNKey(bases, keys));
        
        //KEY9
        bases = new ArrayList();
        bases.add(new BinaryPolynomial("1001110101111"));
        bases.add(new BinaryPolynomial("1011011"));
        bases.add(new BinaryPolynomial("1110101"));
        bases.add(new BinaryPolynomial("1000011"));
        bases.add(new BinaryPolynomial("1110011"));
        bases.add(new BinaryPolynomial("111011"));
        bases.add(new BinaryPolynomial("100101"));
        bases.add(new BinaryPolynomial("11001"));
        bases.add(new BinaryPolynomial("10011"));
        bases.add(new BinaryPolynomial("11111"));
        bases.add(new BinaryPolynomial("1101"));
        bases.add(new BinaryPolynomial("1011"));
        
        keys = new ArrayList();
        keys.add(new BinaryPolynomial("101011011010"));
        keys.add(new BinaryPolynomial("101000"));
        keys.add(new BinaryPolynomial("110010"));
        keys.add(new BinaryPolynomial("101000"));
        keys.add(new BinaryPolynomial("101100"));
        keys.add(new BinaryPolynomial("11000"));
        keys.add(new BinaryPolynomial("0‭0011"));
        keys.add(new BinaryPolynomial("1010"));
        keys.add(new BinaryPolynomial("1111"));
        keys.add(new BinaryPolynomial("0011"));
        keys.add(new BinaryPolynomial("011"));
        keys.add(new BinaryPolynomial("111"));
                
        fullkeys.add(new NPNKey(bases, keys));
        
        //KEY10
        bases = new ArrayList();
        bases.add(new BinaryPolynomial("11110001"));
        bases.add(new BinaryPolynomial("1000011"));
        bases.add(new BinaryPolynomial("1110101"));
        bases.add(new BinaryPolynomial("100101"));
        bases.add(new BinaryPolynomial("110111"));
        bases.add(new BinaryPolynomial("101111"));
        bases.add(new BinaryPolynomial("111101"));
        bases.add(new BinaryPolynomial("101001"));
        bases.add(new BinaryPolynomial("11001"));
        bases.add(new BinaryPolynomial("11111"));
        bases.add(new BinaryPolynomial("10011"));
        bases.add(new BinaryPolynomial("1101"));
        bases.add(new BinaryPolynomial("1011"));
        bases.add(new BinaryPolynomial("111"));
        
        keys = new ArrayList();
        keys.add(new BinaryPolynomial("0010001"));
        keys.add(new BinaryPolynomial("111111"));
        keys.add(new BinaryPolynomial("001010"));
        keys.add(new BinaryPolynomial("10111"));
        keys.add(new BinaryPolynomial("0‭0101"));
        keys.add(new BinaryPolynomial("01100"));
        keys.add(new BinaryPolynomial("0‭0110"));
        keys.add(new BinaryPolynomial("0‭1110"));
        keys.add(new BinaryPolynomial("1110"));
        keys.add(new BinaryPolynomial("0110"));
        keys.add(new BinaryPolynomial("0100"));
        keys.add(new BinaryPolynomial("011"));
        keys.add(new BinaryPolynomial("001"));
        keys.add(new BinaryPolynomial("01"));
                
        fullkeys.add(new NPNKey(bases, keys));
        
        for(NPNKey fullKey: fullkeys) {
           // System.out.println("KEY #####");
            for(BinaryPolynomial base: fullKey.getBases()) {
               // System.out.println("Using base " + base + " " + base.getDegree());
            }
            // System.out.println("##### ");
        }
       
        return fullkeys;
    }
    
//    public static void main(String[] args) {
//
//        try {
//            // This will load the MySQL driver, each DB has its own driver
//            Class.forName("com.mysql.jdbc.Driver");
//            // Setup the connection with the DB
//            connect = DriverManager
//                    .getConnection("jdbc:mysql://localhost/crypto?"
//                            + "user=root&password=mysql");
//
//            // Statements allow to issue SQL queries to the database
//            statement = connect.createStatement();
//
//            int d = 33;
//
//            long n = (int) Math.pow(2, d);
//            for (long i = 33025; i < n; i++) {
//
//                BinaryPolynomial p = new BinaryPolynomial(Long.toBinaryString(i));
//
//                if (p.isIrreducible()) {
//                    System.out.println(p.toString());
//                    // Write polynomial to DB
//                    preparedStatement = connect
//                            .prepareStatement("insert ignore into polynomials values (default, ?, ?)");
//
//                    // Parameters start with 1
//                    preparedStatement.setString(1, String.valueOf(p.getDegree()));
//                    preparedStatement.setString(2, p.toString());
//                    preparedStatement.executeUpdate();
//                }
//            }
//
//            if (statement != null) {
//                statement.close();
//            }
//
//            if (connect != null) {
//                connect.close();
//            }
//
//        } catch (Exception e) {
//            System.out.println("Error: " + e.toString());
//        }
//
//    }

}
