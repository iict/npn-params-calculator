/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polynomial;

import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.util.stream.IntStream;

/**
 *
 * @author Мирас
 */
public class BinaryHelpers {

    public static String toBinaryString(byte[] bytes, int length) {
        StringBuilder sb = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            int byteIdx = i / Byte.SIZE;
            if(bytes.length <= byteIdx) {
                sb.append('0');
            } else {
                sb.append((bytes[byteIdx] << i % Byte.SIZE & 0x80) == 0 ? '0' : '1');
            }
        }
        return sb.toString();
    }

    public static byte[] fromBinaryString(final String s) {
        final int sLen = s.length();
        final byte[] toReturn = new byte[(sLen + Byte.SIZE - 1) / Byte.SIZE];

        IntStream.range(0, sLen).parallel().forEach(i -> {
            if ((s.charAt(i)) == '1') {
                toReturn[i / Byte.SIZE] = (byte) (toReturn[i / Byte.SIZE] | (0x80 >>> (i % Byte.SIZE)));
            }
        });
        return toReturn;
    }

    public static byte[] shiftBitsLeft(byte[] bytes, final int shifts) {
        
        // create from array
        BigInteger bigInt = new BigInteger(bytes);

        // shift
        BigInteger shiftInt = bigInt.shiftLeft(shifts);

        // back to array
        return shiftInt.toByteArray();
     }

    public static byte[] intArrayToByteArray(int[] input) {

        ByteBuffer byteBuffer = ByteBuffer.allocate(input.length * 4);
        IntBuffer intBuffer = byteBuffer.asIntBuffer();
        intBuffer.put(input);

        return byteBuffer.array();

    }
    
    public static int[] byteArrayToIntArray(byte[] input) {
        int[] ret = new int[input.length];
        for (int i = 0; i < input.length; i++)
        {
            ret[i] = input[i] & 0xff; // Range 0 to 255, not -128 to 127
        }
        return ret;
    }

    public static byte[] concat(byte[] a, byte[] b) {
        int aLen = a.length;
        int bLen = b.length;
        byte[] c = new byte[aLen + bLen];
        System.arraycopy(a, 0, c, 0, aLen);
        System.arraycopy(b, 0, c, aLen, bLen);
        return c;
    }
    
  
    public static int byteArrayToInt(byte[] b) {
        return   b[0] & 0xFF |
                (b[1] & 0xFF) << 8 |
                (b[2] & 0xFF) << 16 |
                (b[3] & 0xFF) << 24;
    }
}
